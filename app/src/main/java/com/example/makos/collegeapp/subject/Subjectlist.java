package com.example.makos.collegeapp.subject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.example.makos.collegeapp.R;
import com.example.makos.collegeapp.chapter.Chapter_list;

import java.util.ArrayList;

public class Subjectlist extends AppCompatActivity implements SubjectPresenter.subjects {
    CardView cardview;
    SubjectPresenter presenter;
    RecyclerView recsubjectlist;
    SubjectlistAdaptor adaptor;
    ProgressDialog dialog;
    RecyclerView.LayoutManager manager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subjectlist);
        cardview=findViewById(R.id.cardview);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        recsubjectlist = findViewById(R.id.subjectlist);
        manager = new LinearLayoutManager(Subjectlist.this);
        recsubjectlist.setLayoutManager(manager);
dialog  = new ProgressDialog(Subjectlist.this);
dialog.setTitle("Loading...");
dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
dialog.setMessage("Please wait.....");
       /* cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(), Chapter_list.class);
                startActivity(intent);
            }
        });*/
        presenter = new SubjectPresenter(Subjectlist.this,this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        dialog.show();
        presenter.subjectapi();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showProgressbar() {

    }

    @Override
    public void hideProgressbar() {
     dialog.dismiss();
    }

    @Override
    public void show_examSubjects(ArrayList<subjectModel> models) {
        Log.d("size", String.valueOf(models.size()));
         adaptor = new SubjectlistAdaptor(models,Subjectlist.this,presenter);
         recsubjectlist.setAdapter(adaptor);
         adaptor.notifyDataSetChanged();
    }
}
