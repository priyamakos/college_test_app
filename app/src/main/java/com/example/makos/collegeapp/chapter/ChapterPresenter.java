package com.example.makos.collegeapp.chapter;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.makos.collegeapp.NetworkInfo.Connectivity;
import com.example.makos.collegeapp.mysingleton.Mysingleton;
import com.example.makos.collegeapp.statusurl.Staticurl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ChapterPresenter {
    ArrayList<testlistModel> testlistModels;
    Chapter_list chapter_list;
    Context context;

    public ChapterPresenter(Context context,Chapter_list chapterList)
    {
        this.chapter_list = chapterList;
        this.context = context;
    }

    public  void test_list_api(String examid)
    {
        String url = Staticurl.test_list+"?exam_id="+examid;
        Log.d("url",url);
        if(Connectivity.isConnected(context))
        {
            JsonArrayRequest examlistrequest = new JsonArrayRequest(Request.Method.GET,url,null,
                    new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray jsonObject) {

                            Log.e("jsonObject", String.valueOf(jsonObject));
                            try {
                                JSONObject obj = jsonObject.getJSONObject(0);
                             if(obj.getString("Status").equals("True")) {
                                   Log.d("in",obj.getJSONArray("Test").toString());
                                   JSONArray jsontest = obj.getJSONArray("Test");
                                   testlistModels = fetch_tests(jsontest);
                                   Log.d("jsonarray",jsontest.toString());
                                   chapter_list.show_tests(testlistModels);
                                   chapter_list.hideProgressBar();
                             }//  subjectlist.show_examSubjects(subjectModels);

                            } catch (Exception e) {
                                Log.d("jsonerror",e.getMessage());
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error)
                        {
                    chapter_list.hideProgressBar();

                            NetworkResponse networkResponse = error.networkResponse;
                            if (networkResponse != null && networkResponse.data != null) {
                                String jsonError = new String(networkResponse.data);
                                Log.e("jsonError",jsonError);
                                try {
                                    JSONObject jsonObject = new JSONObject(jsonError);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                    });
            Mysingleton.getInstance(context).addToRequestQueue(examlistrequest);
        }
        else {
            Toast.makeText(context,"Please Check Internet",Toast.LENGTH_SHORT).show();
        }
    }

    ArrayList<testlistModel> fetch_tests(JSONArray testlistreponse) {
        testlistModels = new ArrayList<>();
        try {
        for (int k = 0; k < testlistreponse.length(); k++) {

                JSONObject obj = testlistreponse.getJSONObject(k);
                testlistModel testlistModel = new testlistModel();
                testlistModel.setTest_id(obj.getString("test_id"));
                testlistModel.setTest_name(obj.getString("test_name"));
                testlistModel.setTest_hrs(obj.getString("test_hrs"));
                testlistModels.add(testlistModel);


        }
        Log.d("size", String.valueOf(testlistModels.size()));
            return testlistModels;
        } catch (JSONException e) {
            e.printStackTrace();
            return testlistModels;
        }
    }

    public interface  Chapterlistener{
        void showProgressBar();
        void hideProgressBar();
        void show_tests(ArrayList<testlistModel> models);
    }
}
