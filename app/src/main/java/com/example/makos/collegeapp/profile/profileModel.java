package com.example.makos.collegeapp.profile;

public class profileModel {
    String student_name,std,college_name,email_id,mobile_no,payment_amount,payment_status,student_id;

    public String getstudent_name() {
        return student_name;
    }

    public void setstudent_name(String name) {
        this.student_name = name;
    }

    public String getPayment_status() {
        return payment_status;
    }

    public void setPayment_status(String payment_status) {
        this.payment_status = payment_status;
    }

    public String getStd() {
        return college_name;

    }

    public void setStd(String std) {
        this.college_name = std;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public String getCollege_name() {
        return college_name;
    }

    public void setCollege_name(String college_name) {
        this.college_name = college_name;
    }

    public String getEmail_id() {
        return email_id;
    }

    public void setEmail_id(String email_id) {
        this.email_id = email_id;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getPayment_amount() {
        return payment_amount;
    }

    public void setPayment_amount(String payment_amount) {
        this.payment_amount = payment_amount;
    }



    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getcollege_name() {
        return college_name;

    }

    public void setcollege_name(String collegename) {
        this.college_name = collegename;
    }

}
