package com.example.makos.collegeapp.profile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.makos.collegeapp.R;
import com.example.makos.collegeapp.dialog.LoadingDialog;
import com.example.makos.collegeapp.sign_up.Sign_UP;
import com.example.makos.collegeapp.staticConstants.staticConstants;

import java.io.ByteArrayOutputStream;

/**
 * Created by pc2 on 10-12-2018.
 */

public class Profile extends AppCompatActivity implements profilePresenter.profilelistener {
    LoadingDialog dialog;
    profilePresenter presenter;
    TextView stdname, schname,email,mobile;
    ImageView rprofimg,editprofile;
    Bitmap bitmap;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    public static final int PICK_IMAGE = 1;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);
        Toolbar toolbar = findViewById(R.id.toolbar);

        stdname = findViewById(R.id.name);
        schname = findViewById(R.id.schoolname);
        pref = getSharedPreferences(staticConstants.TAG,MODE_PRIVATE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        dialog = new LoadingDialog(Profile.this, "Loading");
        presenter = new profilePresenter(Profile.this, this);
        rprofimg = findViewById(R.id.rprofimg);
        editprofile = findViewById(R.id.editprofile);
        mobile = findViewById(R.id.mobileno);
        email = findViewById(R.id.email);


        if(pref.contains(staticConstants.profileimg))
        {
            if(!pref.getString(staticConstants.profileimg,"").equals("")){
                byte[] decodedString = Base64.decode(pref.getString(staticConstants.profileimg,""), Base64.DEFAULT);
                Bitmap bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                rprofimg.setImageBitmap(bitmap);
            }
        }

     onclick_events();


    }

    // Click Event functions

    private void onclick_events() {

        editprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                  Intent intent = new Intent(Profile.this, Sign_UP.class);
                  intent.putExtra("school_name",schname.getText().toString());
                  intent.putExtra("mobile_no",mobile.getText().toString());
                  intent.putExtra("student_name",stdname.getText().toString());
                  intent.putExtra("email_id",email.getText().toString());
                  startActivity(intent);
            }
        });

        rprofimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.profile_api();
        showProgressbar();
    }

    @Override
    public void hideProgressbar() {
        dialog.dismiss();
    }


    @Override
    public void showProgressbar() {
        dialog.show();
    }


    @Override
    public void showprofile(profileModel model) {
        hideProgressbar();
        if (model != null) {

            stdname.setText(model.getstudent_name());
            schname.setText(model.getCollege_name());
            mobile.setText(model.getMobile_no());
            email.setText(model.getEmail_id());
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE && data != null) {
            Uri selectedImage = data.getData();
            String fileId = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                fileId = DocumentsContract.getDocumentId(selectedImage);
                String id = fileId.split(":")[1];
                Log.d("selectedImage", selectedImage.toString() + "," + id);
                String selector = MediaStore.Images.Media._ID + "=?";
                Log.d("selector", selector);
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        filePathColumn, selector, new String[]{id}, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                Log.d("id", "index=" + columnIndex);
                bitmap = BitmapFactory.decodeFile(cursor.getString(columnIndex));
                rprofimg.setImageBitmap(bitmap);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                byte[] b = baos.toByteArray();
                String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
                editor = pref.edit();
                editor.putString(staticConstants.profileimg,encodedImage);
                editor.commit();
                //    path = cursor.getString(columnIndex);
            }
        }
    }
}
