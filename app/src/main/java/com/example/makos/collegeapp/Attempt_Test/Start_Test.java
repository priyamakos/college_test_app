package com.example.makos.collegeapp.Attempt_Test;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.makos.collegeapp.R;

public class Start_Test extends AppCompatActivity
{
    TextView next;
    ImageView status;

    CardView opt1,opt2,opt3,opt4;

    int stat1 = 0,stat2 = 0,stat3 = 0,stat4 = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start__test);

        status = findViewById(R.id.status);
        next = findViewById(R.id.next);

       /* opt1 = findViewById(R.id.opt1);
        opt2 = findViewById(R.id.opt2);
        opt3 = findViewById(R.id.opt3);
        opt4= findViewById(R.id.opt4);

        opt1.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v)
            {
                if (stat1 == 0)
                {
                    stat1 = 1;
                    opt1.setCardBackgroundColor(R.color.green);
                }
                else
                {
                    stat1 = 0;
                    opt1.setCardBackgroundColor(R.color.grey);
                }
            }
        });

        opt2.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v)
            {
                if (stat2 == 0)
                {
                    stat2 = 1;
                    opt2.setCardBackgroundColor(R.color.green);
                }
                else
                {
                    stat2 = 0;
                    opt2.setCardBackgroundColor(R.color.grey);
                }
            }
        });

        opt3.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v)
            {
                if (stat3 == 0)
                {
                    stat3 = 1;
                    opt3.setCardBackgroundColor(R.color.green);
                }
                else
                {
                    stat3 = 0;
                    opt3.setCardBackgroundColor(R.color.grey);
                }
            }
        });

        opt4.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v)
            {
                if (stat4 == 0)
                {
                    stat4 = 1;
                    opt4.setCardBackgroundColor(R.color.green);
                }
                else
                {
                    stat4 = 0;
                    opt4.setCardBackgroundColor(R.color.grey);
                }
            }
        });*/

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(Start_Test.this,Show_Result.class);
                startActivity(intent);
            }
        });

        status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                popup();
            }
        });

        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }

    private void popup()
    {
        final Dialog dialog = new Dialog(Start_Test.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.status_dialog);

        Button ok = dialog.findViewById(R.id.ok);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
