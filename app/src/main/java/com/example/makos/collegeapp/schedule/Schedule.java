package com.example.makos.collegeapp.schedule;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.makos.collegeapp.MainActivity;
import com.example.makos.collegeapp.R;
import com.example.makos.collegeapp.question.Question;
import com.example.makos.collegeapp.staticConstants.staticConstants;

import java.util.Calendar;

/**
 * Created by pc2 on 10-12-2018.
 */

public class Schedule extends AppCompatActivity implements SchedulePresenter.Schedulelistener{
    ImageView date,time_img;
    Button submit;
    TextView selected_date,selected_time;
    RelativeLayout date_layout,time_layout;

    private DatePicker datePicker;
    private Calendar calendar;
    private int year, month, day,time;
    private DatePickerDialog datePickerDialog;
    TimePickerDialog mTimePicker;
    TextView exam_name,test_name;
    String test_id,student_id;
    SharedPreferences pref;
    Intent intent;
    ProgressDialog dialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.schedule);
        intent = getIntent();

        submit = findViewById(R.id.sumbit);
        date = findViewById(R.id.date_img);
        time_img = findViewById(R.id.time_img);
        selected_date = findViewById(R.id.selected_date);
        selected_time = findViewById(R.id.selected_time);
        date_layout = findViewById(R.id.date_layout);
        time_layout = findViewById(R.id.time_layout);
        exam_name = findViewById(R.id.exam_name);
        test_name = findViewById(R.id.test_name);

        dialog = new ProgressDialog(Schedule.this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("Please wait...");
        dialog.setTitle("Loading");

        if (intent != null)
        {
            exam_name.setText(intent.getStringExtra("exam_name"));
            test_name.setText(intent.getStringExtra("test_name"));
            test_id = intent.getStringExtra("test_id");
        }

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        selected_date.setInputType(InputType.TYPE_NULL);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
      final SchedulePresenter presenter =  new SchedulePresenter(Schedule.this,this);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressBar();
                presenter.schdule_api(test_id,selected_time.getText().toString(),selected_date.getText().toString(),student_id);
                //Intent in=new Intent(getApplicationContext(), Question.class);
                //startActivity(in);
            }
        });

        date_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDate();
            }
        });


        selected_time.setInputType(InputType.TYPE_NULL);
        selected_time.setFocusable(false);
        time_layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Calendar mcurrentTime = Calendar.getInstance();

                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                mTimePicker = new TimePickerDialog(Schedule.this, new TimePickerDialog.OnTimeSetListener()
                {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute)
                    {
                        String am_pm = "hi";
                        Log.d("am_pmhi",am_pm);
                        if (selectedHour >=0 && selectedHour < 12){
                            am_pm = "AM";
                            Log.d("inam_pm",am_pm);
                        } else {
                            am_pm = "PM";
                        }
                        Log.d("am_pm",am_pm);
                        selected_time.setText(selectedHour + ":" + selectedMinute  );
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });


    }


    public void setDate()
    {
        showDialog(999);
        //Toast.makeText(getApplicationContext(), "ca", Toast.LENGTH_SHORT).show();
    }
    @Override
    protected Dialog onCreateDialog(int id)
    {
        // TODO Auto-generated method stub
        if (id == 999)
        {
            datePickerDialog = new DatePickerDialog(this, myDateListener, year, month, day);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
            return datePickerDialog;
        }
        return null;
    }
    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener()
    {
        @SuppressLint("ResourceAsColor")
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3)
        {
            // TODO Auto-generated method stub
            // arg1 = year
            // arg2 = month
            // arg3 = day
            arg0.setBackgroundColor(R.color.red);
            arg0.setMinDate(System.currentTimeMillis());
            showDate(arg1, arg2+1, arg3);
        }
    };


    private void showDate(int year, int month, int day)
    {
        selected_date.setText(new StringBuilder().append(day).append("-").append(month).append("-").append(year));
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showProgressBar() {
           dialog.show();
    }

    @Override
    public void hideProgressBar() {
         dialog.dismiss();
    }

    @Override
    public void message(String message,String status) {
        if (status.equals("True")) {
            Toast.makeText(Schedule.this, message, Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(Schedule.this, MainActivity.class);
            startActivity(intent);
        }
    }
}
