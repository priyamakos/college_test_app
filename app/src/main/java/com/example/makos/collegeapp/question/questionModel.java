package com.example.makos.collegeapp.question;

public class questionModel {
    String question_id,question_name,question_image,objective1,objective2,objective3,objective4;
    String Status,Message;

    public String getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(String question_id) {
        this.question_id = question_id;
    }

    public String getQuestion_name() {
        return question_name;
    }

    public void setQuestion_name(String question_name) {
        this.question_name = question_name;
    }

    public String getQuestion_image() {
        return question_image;
    }

    public void setQuestion_image(String question_image) {
        this.question_image = question_image;
    }

    public String getObjective1() {
        return objective1;
    }

    public void setObjective1(String objective1) {
        this.objective1 = objective1;
    }

    public String getObjective2() {
        return objective2;
    }

    public void setObjective2(String objective2) {
        this.objective2 = objective2;
    }

    public String getObjective3() {
        return objective3;
    }

    public void setObjective3(String objective3) {
        this.objective3 = objective3;
    }

    public String getObjective4() {
        return objective4;
    }

    public void setObjective4(String objective4) {
        this.objective4 = objective4;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }
}
