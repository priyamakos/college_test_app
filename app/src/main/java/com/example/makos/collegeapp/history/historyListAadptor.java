package com.example.makos.collegeapp.history;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.makos.collegeapp.R;
import com.example.makos.collegeapp.schedule_list.scheduleListModel;

import java.util.ArrayList;

public class historyListAadptor extends RecyclerView.Adapter<historyListAadptor.MyViewHolder> {
    ArrayList<historyListModel> scheduleListModels;
    Context context;
    TextView total_marks,date,test_name;
    public historyListAadptor(Context context, ArrayList<historyListModel> scheduleListModels)
    {
        this.scheduleListModels = scheduleListModels;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
     View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_history_layout,viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        test_name = myViewHolder.test_name;
        date = myViewHolder.date;
        total_marks = myViewHolder.total_marks;
        historyListModel model = scheduleListModels.get(i);
        test_name.setText(model.getTest_name());
        total_marks.setText(model.getTotal_marks()+"/"+model.getExam_marks());

    }

    @Override
    public int getItemCount() {
        return scheduleListModels.size();
    }

    public class  MyViewHolder extends RecyclerView.ViewHolder{
 TextView test_name,exam_name,total_marks,exam_marks,date;
 Button start;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            test_name = itemView.findViewById(R.id.test_name);
            date = itemView.findViewById(R.id.date);
            total_marks = itemView.findViewById(R.id.marks);

        }
    }
}
