package com.example.makos.collegeapp.history;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.makos.collegeapp.R;
import com.example.makos.collegeapp.q_answer.Q_Answer;
import com.example.makos.collegeapp.question.Question;
import com.example.makos.collegeapp.schedule_list.scheduleListModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class historyHomeadaptor extends RecyclerView.Adapter<historyHomeadaptor.Myviewholder> {
   Context context;
   Historypresenter historypresenter;
   ArrayList<historyHomemodel> homemodels;
    TextView time,date,test_name,start;
    public  historyHomeadaptor(Context context, ArrayList<historyHomemodel> homemodels,Historypresenter presenter){
        this.context = context;
        this.homemodels = homemodels;
        this.historypresenter = presenter;

    }
    @NonNull
    @Override
    public Myviewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_history_home,viewGroup,false);
        return new Myviewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Myviewholder myViewHolder,final int i) {
        start = myViewHolder.start;
        test_name = myViewHolder.test_name;
        date = myViewHolder.date;
        time = myViewHolder.time;

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy");

        final historyHomemodel model = homemodels.get(i);
        try {

            Date dates = format.parse(model.getExam_date());
            date.setText(format1.format(dates));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        test_name.setText(model.getTest_name());

        time.setText(model.getExam_time());
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                historyHomemodel model = homemodels.get(i);
                Intent in=new Intent(context, Q_Answer.class);
                in.putExtra("test_id",model.getTest_id());
                in.putExtra("exam_id",model.getExam_id());
                context.startActivity(in);

            }
        });
    }

    @Override
    public int getItemCount() {
        return homemodels.size();
    }

    public class Myviewholder extends RecyclerView.ViewHolder{
        TextView test_name,date,time,start;
        //Button start;
    public Myviewholder(@NonNull View itemView) {

        super(itemView);
        test_name = itemView.findViewById(R.id.test_name);
        date = itemView.findViewById(R.id.date);
        time = itemView.findViewById(R.id.time);
        start = itemView.findViewById(R.id.start);
    }
}
}
