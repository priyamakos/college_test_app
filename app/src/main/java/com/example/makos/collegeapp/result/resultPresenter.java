package com.example.makos.collegeapp.result;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.makos.collegeapp.NetworkInfo.Connectivity;
import com.example.makos.collegeapp.database.DatabaseHandler;
import com.example.makos.collegeapp.history.historyHomemodel;
import com.example.makos.collegeapp.mysingleton.Mysingleton;
import com.example.makos.collegeapp.q_answer.q_Answermodel;
import com.example.makos.collegeapp.question.Question;
import com.example.makos.collegeapp.question.questionModel;
import com.example.makos.collegeapp.statusurl.Staticurl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class resultPresenter {
    Context context;
    Result question;
    DatabaseHandler handler;

    ArrayList<questionModel> questionModelArrayList,arrayList;

    public resultPresenter(Context context,Result question)
    {
        this.context = context;
        this.question = question;
        this.questionModelArrayList = new ArrayList<>();
        handler =   new DatabaseHandler(context);

    }

/*    public void resultapi()
    {
        String url = Staticurl.baseurl+"/question_list?exam_id=1&test_id=1";
        Log.d(context.getPackageName()+"/url",url);

        if(Connectivity.isConnected(context))
        {
            JsonArrayRequest examlistrequest = new JsonArrayRequest(Request.Method.GET,url,null,
                    new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray jsonObject) {

                            Log.e("jsonObject", String.valueOf(jsonObject));
                            try {
                                JSONObject obj = jsonObject.getJSONObject(0);
                                if(obj.getString("Status").equals("True")) {
                                    //  handler.trucate_table();
                                    fetch_qutions(obj.getJSONArray("Question List Data"));
                                    fetch_first_question(0);
                                    Question.selops = new String[obj.getJSONArray("Question List Data").length()];
                                }
                                else {
                                    Toast.makeText(context,obj.getString("Message"),Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {
                                Log.d("jsonerror",e.getMessage());
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error)
                        {
                            question.hideProgressbar();

                            NetworkResponse networkResponse = error.networkResponse;
                            if (networkResponse != null && networkResponse.data != null) {
                                String jsonError = new String(networkResponse.data);
                                Log.e("jsonError",jsonError);
                                try {
                                    JSONObject jsonObject = new JSONObject(jsonError);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                    });
            Mysingleton.getInstance(context).addToRequestQueue(examlistrequest);
        }
        else {
            Toast.makeText(context,"Please Check Internet",Toast.LENGTH_SHORT).show();
        }
    }*/
public interface q_resultliseners{
    void hideProgressbar();
    void showProgressbar();
   // void historyList(ArrayList<q_Answermodel> historyListModels);
   // void historyhomeList(ArrayList<historyHomemodel> historyHomemodels);
}
}
