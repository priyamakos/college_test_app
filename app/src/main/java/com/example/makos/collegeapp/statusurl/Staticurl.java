package com.example.makos.collegeapp.statusurl;

/**
 * Created by pc2 on 12-12-2018.
 */

public class Staticurl {


    public static String baseurl="http://jeeapplication-env.mmxqihrcus.ap-south-1.elasticbeanstalk.com/collegeapp";

    public static String signup= baseurl +"/signup"; //?student_name=ajit&college_name=xyz&mobile_no=9112133465&email_id=ajit@gmail.com&password=123";

    public static String login=baseurl +"/login"; //?username=gkalkeri@gmail.com&password=123";

    public static String  forgot_password= baseurl+"/forgot_pass";//?username=gkalkeri@gmail.com";

    public static  String changed_password= baseurl +"/change_password";//?student_id=1&old_password=123&new_password=321";

    public  static String exam_name_list=baseurl +"/exam_list";

    public static String test_list=baseurl +"/test_wise_exam_list";//?exam_id=1";

    public static String schedule_test=baseurl +"/test_schedule";//?student_id=1&exam_id=1&test_id=1&exam_time=03:10:00&exam_date=2018-10-10";

    public static String  schedule_test_list=baseurl +"/scheduled_list";//?student_id=1";

    public static String  logout=baseurl +"/logout?student_id=";

    public static String update_profile=baseurl+"/update_profile";
}
