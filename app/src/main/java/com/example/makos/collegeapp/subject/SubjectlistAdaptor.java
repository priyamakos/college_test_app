package com.example.makos.collegeapp.subject;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.makos.collegeapp.R;
import com.example.makos.collegeapp.chapter.Chapter_list;

import java.util.ArrayList;

public class SubjectlistAdaptor extends RecyclerView.Adapter<SubjectlistAdaptor.SubjectlistViewholder> {
    ArrayList<subjectModel> subjectModels;
    Context context;
    SubjectPresenter presenter;
 public    static String exam_name="",exam_id;
    public SubjectlistAdaptor(ArrayList<subjectModel> subjectModels, Context mcontext,SubjectPresenter presenter)
    {
        this.subjectModels = subjectModels;
        this.context = mcontext;
        this.presenter = presenter;
    }
    @NonNull
    @Override
    public SubjectlistViewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_list_subejcts,viewGroup,false);
        return new SubjectlistViewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SubjectlistViewholder subjectlistViewholder, final int i) {
            TextView txtexamname = subjectlistViewholder.examname;
            subjectModel model = subjectModels.get(i);
            txtexamname.setText(model.getExam_name());
            txtexamname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, Chapter_list.class);
                    intent.putExtra("exam_id",subjectModels.get(subjectlistViewholder.getAdapterPosition()).getExam_id());
                    exam_name = subjectModels.get(i).getExam_name();
                    exam_id = subjectModels.get(subjectlistViewholder.getAdapterPosition()).getExam_id();
                    v.getContext().startActivity(intent);
                }
            });
    }

    @Override
    public int getItemCount() {
        return subjectModels.size();
    }

    public class  SubjectlistViewholder extends RecyclerView.ViewHolder{
             TextView examname;
        public SubjectlistViewholder(@NonNull View itemView) {

            super(itemView);
            examname = itemView.findViewById(R.id.exam_name);
        }
    }
}
