package com.example.makos.collegeapp.subject;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.makos.collegeapp.NetworkInfo.Connectivity;
import com.example.makos.collegeapp.chapter.testlistModel;
import com.example.makos.collegeapp.mysingleton.Mysingleton;
import com.example.makos.collegeapp.statusurl.Staticurl;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SubjectPresenter {
    Context context;
    Subjectlist subjectlist;
    ArrayList<testlistModel> testlistModels;
    ArrayList<subjectModel> subjectModels;

public SubjectPresenter(Context context,Subjectlist list )
{
    this.context = context;
    this.subjectlist = list;

}

    public interface subjects{
        void showProgressbar();
        void hideProgressbar();
        void show_examSubjects(ArrayList<subjectModel> models);
    }


   public void subjectapi()
    {
        if(Connectivity.isConnected(context))
        {
        JsonArrayRequest examlistrequest = new JsonArrayRequest(Request.Method.GET,Staticurl.exam_name_list,null,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray jsonObject) {

                        Log.e("jsonObject", String.valueOf(jsonObject));
                        try {
                         subjectModels= fetch_subjects(jsonObject);
                            subjectlist.show_examSubjects(subjectModels);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        //progressDialog.dismiss();

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            Log.e("jsonError",jsonError);
                            try {
                                JSONObject jsonObject = new JSONObject(jsonError);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                });
            Mysingleton.getInstance(context).addToRequestQueue(examlistrequest);
        }
        else {
            Toast.makeText(context,"Please Check Internet",Toast.LENGTH_SHORT).show();
        }

    }

    ArrayList<subjectModel> fetch_subjects(JSONArray jsonArray) {
        subjectModels = new ArrayList<>();
        try {
            JSONObject obj = jsonArray.getJSONObject(0);
            Log.d("array",jsonArray.toString());
            if(obj.getString("Status").equals("True")) {
                JSONArray exam_list_data = obj.getJSONArray("Exam List Data");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject exam_list = exam_list_data.getJSONObject(i);
                    subjectModel model = new subjectModel();
                    model.setExam_id(exam_list.getString("exam_id"));
                    model.setExam_name(exam_list.getString("exam_name"));
                    model.setExam_marks(exam_list.getString("exam_marks"));
                    model.setNo_of_questions(exam_list.getString("no_of_questions"));
                    model.setNegative_marks(exam_list.getString("negative_marks"));
                    subjectModels.add(model);
                }
                subjectlist.hideProgressbar();
            }
            return subjectModels;
        } catch (JSONException e) {
            e.printStackTrace();
            return subjectModels;
        }
    }

}
