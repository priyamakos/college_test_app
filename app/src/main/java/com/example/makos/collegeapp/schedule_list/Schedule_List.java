package com.example.makos.collegeapp.schedule_list;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.makos.collegeapp.R;
import com.example.makos.collegeapp.question.Question;

import org.json.JSONArray;

import java.util.ArrayList;

public class Schedule_List extends AppCompatActivity implements ScheduleListPresenter.ScheduleListener {
  //  Button start;
RecyclerView schedule_list;
ProgressDialog dialog;
RecyclerView.LayoutManager manager;
scheduleListAadptor aadptor;
    ScheduleListPresenter presenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule__list);
      //  start=findViewById(R.id.start);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        schedule_list = findViewById(R.id.recsch_list);
        manager = new LinearLayoutManager(Schedule_List.this);
        schedule_list.setLayoutManager(manager);
        dialog = new ProgressDialog(Schedule_List.this);
        dialog.setTitle("Loading");
        dialog.setMessage("Please Wait...");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        presenter = new ScheduleListPresenter(Schedule_List.this,this);

     /*   start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(getApplicationContext(), Question.class);
                startActivity(in);
            }
        });*/
    }

    @Override
    protected void onResume() {
        presenter.setSchedule_listapi();
        showProgressbar();
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showProgressbar() {
dialog.show();
    }

    @Override
    public void hideProgressbar() {
dialog.dismiss();
    }

    @Override
        public void scheduleList(ArrayList<scheduleListModel> scheduleListModels) {
        aadptor = new scheduleListAadptor(Schedule_List.this,scheduleListModels,presenter);
        aadptor.notifyDataSetChanged();
        schedule_list.setAdapter(aadptor);

        hideProgressbar();

    }


}
