package com.example.makos.collegeapp.question;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;

import com.example.makos.collegeapp.R;
import com.example.makos.collegeapp.history.historyListAadptor;

import java.util.ArrayList;

public class questionAdaptor extends RecyclerView.Adapter<questionAdaptor.MyViewHolder> implements View.OnClickListener {

    Context context;
    int sr_q=0;

    TextView question_id,ans1,ans2,ans3,ans4,question;
    ImageView q_img,ans_img1,ans_img2,ans_img3,ans_img4;

    ArrayList<questionModel> questionModels;
    RadioButton q_num1,q_num2,q_num3,q_num4;

    questionPresenter presenter;
    int pos;

   public static String qselflag;
    public questionAdaptor(Context context, ArrayList<questionModel> questionModels, questionPresenter presenter, int pos){
        this.context = context;
        this.questionModels = questionModels;
        this.presenter = presenter;
        this.pos = pos  ;
    }
    @NonNull
    @Override
    public questionAdaptor.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_question_layout,viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull questionAdaptor.MyViewHolder myViewHolder, int i) {

        question_id = myViewHolder.question_id;
        question = myViewHolder.question;
        q_img = myViewHolder.q_img;

        ans1 = myViewHolder.ans1;
        ans2 = myViewHolder.ans2;
        ans3 = myViewHolder.ans3;
        ans4 = myViewHolder.ans4;

        ans_img1 = myViewHolder.ans_img1;
        ans_img2 = myViewHolder.ans_img2;
        ans_img3 = myViewHolder.ans_img3;
        ans_img4 = myViewHolder.ans_img4;

        q_num1 = myViewHolder.q_num1;
        q_num2 = myViewHolder.q_num2;
        q_num3 = myViewHolder.q_num3;
        q_num4 = myViewHolder.q_num4;

        questionModel model = questionModels.get(i);
        //sr_q = sr_q+1;
        question_id.setText(String.valueOf(pos));


        question.setText(model.getQuestion_name());
        ans1.setText(model.getObjective1());
        ans2.setText(model.getObjective2());
        ans3.setText(model.getObjective3());
        ans4.setText(model.getObjective4());

        q_num1.setOnClickListener(this);
        q_num2.setOnClickListener(this);
        q_num4.setOnClickListener(this);
        q_num3.setOnClickListener(this);

    }

    @Override
    public int getItemCount() {
        return questionModels.size();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.q_num1:
                qselflag = ans1.getText().toString();
                q_num2.setChecked(false);
                q_num3.setChecked(false);
                q_num4.setChecked(false);

                break;
            case R.id.q_num2:
                qselflag = ans2.getText().toString();
                q_num1.setChecked(false);
                q_num3.setChecked(false);
                q_num4.setChecked(false);
                break;
            case R.id.q_num3:
                qselflag = ans3.getText().toString();
                q_num1.setChecked(false);
                q_num2.setChecked(false);
                q_num4.setChecked(false);
                break;
            case R.id.q_num4:
                qselflag = ans4.getText().toString();
                q_num1.setChecked(false);
                q_num2.setChecked(false);
                q_num3.setChecked(false);
                break;

        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
       TextView question_id,ans1,ans2,ans3,ans4,question;
       RadioButton q_num1,q_num2,q_num3,q_num4;
       ImageView q_img,ans_img1,ans_img2,ans_img3,ans_img4;
        public MyViewHolder(@NonNull View itemView) {

            super(itemView);
            question = itemView.findViewById(R.id.question);
            question_id = itemView.findViewById(R.id.question_no);

            ans1 = itemView.findViewById(R.id.ans1);
            ans2 = itemView.findViewById(R.id.ans2);
            ans3 = itemView.findViewById(R.id.ans3);
            ans4 = itemView.findViewById(R.id.ans4);

            ans_img1 = itemView.findViewById(R.id.ans_img1);
            ans_img2 = itemView.findViewById(R.id.ans_img2);
            ans_img3 = itemView.findViewById(R.id.ans_img3);
            ans_img4 = itemView.findViewById(R.id.ans_img4);

            q_num1 = itemView.findViewById(R.id.q_num1);
            q_num2 = itemView.findViewById(R.id.q_num2);
            q_num3 = itemView.findViewById(R.id.q_num3);
            q_num4 = itemView.findViewById(R.id.q_num4);

            q_img = itemView.findViewById(R.id.q_img);

        }
    }
}
