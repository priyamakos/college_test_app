package com.example.makos.collegeapp.q_answer;

import com.example.makos.collegeapp.question.questionModel;
import com.example.makos.collegeapp.result.resultmodel;

import org.json.JSONArray;

import java.util.ArrayList;

public class q_Answermodel {
    String status,Message;
    String exam_name,test_name,exam_marks,total_positive_marks,total_negative_marks,total_marks,passing_marks,test_result,student_answer,system_answer,result,explaination;
    //JSONArray list1;
ArrayList<resultmodel> list1;
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStudent_answer() {
        return student_answer;
    }

    public void setStudent_answer(String student_answer) {
        this.student_answer = student_answer;
    }

    public String getSystem_answer() {
        return system_answer;
    }

    public void setSystem_answer(String system_answer) {
        this.system_answer = system_answer;
    }

    public ArrayList<resultmodel> getList1() {
        return list1;
    }

    public void setList1(ArrayList<resultmodel> list1) {
        this.list1 = list1;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getExplaination() {
        return explaination;
    }

    public void setExplaination(String explaination) {
        this.explaination = explaination;
    }



    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getExam_name() {
        return exam_name;
    }

    public void setExam_name(String exam_name) {
        this.exam_name = exam_name;
    }

    public String getTest_name() {
        return test_name;
    }

    public void setTest_name(String test_name) {
        this.test_name = test_name;
    }

    public String getExam_marks() {
        return exam_marks;
    }

    public void setExam_marks(String exam_marks) {
        this.exam_marks = exam_marks;
    }

    public String getTotal_positive_marks() {
        return total_positive_marks;
    }

    public void setTotal_positive_marks(String total_positive_marks) {
        this.total_positive_marks = total_positive_marks;
    }

    public String getTotal_negative_marks() {
        return total_negative_marks;
    }

    public void setTotal_negative_marks(String total_negative_marks) {
        this.total_negative_marks = total_negative_marks;
    }

    public String getTotal_marks() {
        return total_marks;
    }

    public void setTotal_marks(String total_marks) {
        this.total_marks = total_marks;
    }

    public String getPassing_marks() {
        return passing_marks;
    }

    public void setPassing_marks(String passing_marks) {
        this.passing_marks = passing_marks;
    }

    public String getTest_result() {
        return test_result;
    }

    public void setTest_result(String test_result) {
        this.test_result = test_result;
    }

  /*  public JSONArray getTest_details() {
        return list1;
    }*/

   /* public void setTest_details(JSONArray test_details) {
        this.list1 = test_details;
    }*/
    /*  public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getExam_name() {
        return exam_name;
    }

    public void setExam_name(String exam_name) {
        this.exam_name = exam_name;
    }

    public String getTest_name() {
        return test_name;
    }

    public void setTest_name(String test_name) {
        this.test_name = test_name;
    }

    public String getExam_date() {
        return exam_date;
    }

    public void setExam_date(String exam_date) {
        this.exam_date = exam_date;
    }

    public String getExam_time() {
        return exam_time;
    }

    public void setExam_time(String exam_time) {
        this.exam_time = exam_time;
    }

    public String getIs_attempted() {
        return is_attempted;
    }

    public void setIs_attempted(String is_attempted) {
        this.is_attempted = is_attempted;
    }*/

}

