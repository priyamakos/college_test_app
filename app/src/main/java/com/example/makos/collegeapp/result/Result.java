package com.example.makos.collegeapp.result;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.makos.collegeapp.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class Result extends AppCompatActivity implements resultPresenter.q_resultliseners {
     RecyclerView resultrec;
     LinearLayoutManager manager;
     ArrayList<resultmodel> resultmodels,resultmodels1;
    Intent intent;
    resultPresenter presenter;
    resultAdaptor adaptor;
    Button next;
    int pos=0,size;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        resultrec = findViewById(R.id.resultrec);
        next  = findViewById(R.id.btnnext);
        manager = new LinearLayoutManager(Result.this);
        resultrec.setLayoutManager(manager);
        presenter = new resultPresenter(Result.this,this);
        intent = getIntent();


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             resultmodels1.clear();
             if(pos < size) {
                 resultmodels1.add(resultmodels.get(pos));
                 adaptor.notifyDataSetChanged();
                 pos = pos + 1;
             }
            }
        });
        if(intent!=null)
        {
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<resultmodel>>(){}.getType();
            resultmodels = gson.fromJson(intent.getStringExtra("resultmodel"),type);
        }
        else {
            resultmodels = new ArrayList<>();
        }
             resultmodels1 = new ArrayList<>();
        resultmodels1.add(resultmodels.get(pos));
        adaptor = new resultAdaptor(Result.this,resultmodels1,presenter);
        resultrec.setAdapter(adaptor);
        adaptor.notifyDataSetChanged();
        size = resultmodels.size();
       pos = pos+1;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void hideProgressbar() {

    }

    @Override
    public void showProgressbar() {

    }
}
