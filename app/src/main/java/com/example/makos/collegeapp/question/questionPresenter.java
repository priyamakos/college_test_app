package com.example.makos.collegeapp.question;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.makos.collegeapp.NetworkInfo.Connectivity;
import com.example.makos.collegeapp.database.DatabaseHandler;
import com.example.makos.collegeapp.mysingleton.Mysingleton;
import com.example.makos.collegeapp.staticConstants.staticConstants;
import com.example.makos.collegeapp.statusurl.Staticurl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class questionPresenter {

    Context context;
    Question question;
    DatabaseHandler handler;
    String exam_id,test_id;
    String ques;

    ArrayList<questionModel> questionModelArrayList,arrayList;

    public questionPresenter(Context context,Question question)
    {
        this.context = context;
        this.question = question;
        this.questionModelArrayList = new ArrayList<>();
        handler =   new DatabaseHandler(context);
        ques=  handler.last_attended_ques(question.exam_id,question.test_id);
       if(!ques.equals("0"))
           Question.pos = Integer.parseInt(ques);
       else
           Question.pos = Integer.parseInt("1");



    }

    // calculate and save result calculation done in api directly

    public void save_result(String exam_id,String test_id)
    {
        String url = Staticurl.baseurl+"/save_result?student_id="+ staticConstants.stud_id+"&exam_id="+exam_id+"&test_id="+test_id;
        Log.d(staticConstants.TAG+ question.getLocalClassName()+"/url",url);
        if(Connectivity.isConnected(context))
        {
            JsonArrayRequest saveresult = new JsonArrayRequest(Request.Method.GET,url,null,
                    new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray jsonObject) {

                            Log.e("jsonObject", String.valueOf(jsonObject));
                            try {
                                JSONObject obj = jsonObject.getJSONObject(0);
                                if(obj.getString("Status").equals("True")) {
                                    Toast.makeText(context,obj.getString("Message"),Toast.LENGTH_SHORT).show();
                                    question.finish();
                                }
                                else {
                                    Toast.makeText(context,obj.getString("Message"),Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {
                                Log.d("jsonerror",e.getMessage());
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error)
                        {
                            question.hideProgressbar();

                            NetworkResponse networkResponse = error.networkResponse;
                            if (networkResponse != null && networkResponse.data != null) {
                                String jsonError = new String(networkResponse.data);
                                Log.e("jsonError",jsonError);
                                try {
                                    JSONObject jsonObject = new JSONObject(jsonError);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                    });
            Mysingleton.getInstance(context).addToRequestQueue(saveresult);
        }
        else {
            Toast.makeText(context,"Please Check Internet",Toast.LENGTH_SHORT).show();
        }

    }

    // submit question answer to server

    public void  submit_question(final String test_id,final String exam_id,final String question_d,final String answer)
    {

        String url = Staticurl.baseurl+"/save_student_answer";
           Log.d(staticConstants.TAG+ question.getLocalClassName()+"/url",url);
        if(Connectivity.isConnected(context))
        {
            JsonArrayRequest sumbitquestion = new JsonArrayRequest(Request.Method.GET,url,null,
                    new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray jsonObject) {

                            Log.e("jsonObject", String.valueOf(jsonObject));
                            try {
                                JSONObject obj = jsonObject.getJSONObject(0);
                                if(obj.getString("Status").equals("True")) {
                                    handler.updatequestionoption(exam_id,staticConstants.stud_id,test_id,question_d,answer);

                                }
                                else {
                                    Toast.makeText(context,obj.getString("Message"),Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {
                                Log.d("jsonerror",e.getMessage());
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error)
                        {
                            question.hideProgressbar();

                            NetworkResponse networkResponse = error.networkResponse;
                            if (networkResponse != null && networkResponse.data != null) {
                                String jsonError = new String(networkResponse.data);
                                Log.e("jsonError",jsonError);
                                try {
                                    JSONObject jsonObject = new JSONObject(jsonError);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                    }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String,String> hashMap=new HashMap<>();
                    hashMap.put("student_id",staticConstants.stud_id);
                    hashMap.put("exam_id",exam_id);
                    hashMap.put("test_id",test_id);
                    hashMap.put("question_id",question_d);
                    hashMap.put("answer",answer);
                    return hashMap;
                }
            };
            Mysingleton.getInstance(context).addToRequestQueue(sumbitquestion);
        }
        else {
            Toast.makeText(context,"Please Check Internet",Toast.LENGTH_SHORT).show();
        }

     }

     //fetch all questions of test by using exam_id and test_id

    public void questionApi(final String exam_id, final String test_id)
    {
        String url = Staticurl.baseurl+"/question_list?exam_id="+exam_id+"&test_id="+test_id;
        Log.d(context.getPackageName()+"/url",url);

        if(Connectivity.isConnected(context))
        {
            JsonArrayRequest examlistrequest = new JsonArrayRequest(Request.Method.GET,url,null,
                    new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray jsonObject) {

                            Log.e("jsonObject", String.valueOf(jsonObject));
                            try {
                                JSONObject obj = jsonObject.getJSONObject(0);
                                if(obj.getString("Status").equals("True")) {
                                    fetch_qutions(obj.getJSONArray("Question List Data"),test_id,exam_id);
                                    fetch_first_question(Question.pos);
                                    Question.selops = new String[obj.getJSONArray("Question List Data").length()];
                                }
                                else {
                                    Toast.makeText(context,obj.getString("Message"),Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {
                                Log.d("jsonerror",e.getMessage());
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error)
                        {
                            question.hideProgressbar();
                            NetworkResponse networkResponse = error.networkResponse;

                            if (networkResponse != null && networkResponse.data != null) {
                                String jsonError = new String(networkResponse.data);
                                Log.e("jsonError",jsonError);
                                try {
                                    JSONObject jsonObject = new JSONObject(jsonError);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                    });

            Mysingleton.getInstance(context).addToRequestQueue(examlistrequest);
        }
        else {
            Toast.makeText(context,"Please Check Internet",Toast.LENGTH_SHORT).show();
        }
    }

    // display one question at  time recycleview

    public void fetch_first_question(int pos) {
    arrayList = new ArrayList<>();
    if(pos < questionModelArrayList.size()) {

            arrayList.add(questionModelArrayList.get(pos));
            question.questionbody(arrayList, "Next");

    }else if (pos == 0){
        arrayList.add(questionModelArrayList.get(0));
        question.questionbody(arrayList, "Next");
    }
    else {
        arrayList.add(questionModelArrayList.get(questionModelArrayList.size()-1));
        question.questionbody(arrayList,"Submit");
    }
    }

    //  fetch and put it into model

    void  fetch_qutions(JSONArray questions,String exam_id,String test_id)
    {
          for (int i=0;i<questions.length();i++)
          {
              try {

                  JSONObject ques = questions.getJSONObject(i);
                  questionModel model = new questionModel();
                  model.setQuestion_name(ques.getString("question_name"));
                  model.setQuestion_id(ques.getString("question_id"));
                  model.setObjective1(ques.getString("objective1"));
                  model.setObjective2(ques.getString("objective2"));
                  model.setObjective3(ques.getString("objective3"));
                  model.setObjective4(ques.getString("objective4"));
                  model.setQuestion_image(ques.getString("question_image"));
                  questionModelArrayList.add(model);
                  InsertIndb(staticConstants.stud_id,exam_id,test_id, String.valueOf(i+1),"",handler);

              } catch (JSONException e) {
                  e.printStackTrace();
              }


          }
    }

    private void InsertIndb(String stud_id, String test_id, String exam_id, String question_id, String ans,DatabaseHandler handler) {
        handler.addNewquestion(stud_id,exam_id,test_id,question_id,ans);
    }

    // question view listenters

    public interface  questionlisteners{
        void hideProgressbar();
        void showProgressbar();
        void questionbody(ArrayList<questionModel> questionModels,String btntxt);
    }
}
