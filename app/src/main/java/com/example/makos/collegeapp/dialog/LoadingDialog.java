package com.example.makos.collegeapp.dialog;

import android.app.ProgressDialog;
import android.content.Context;

public class LoadingDialog extends ProgressDialog {
    public LoadingDialog(Context context,String title) {
        super(context);
         this.setProgressStyle(STYLE_SPINNER);
         this.setTitle(title);
         this.setMessage("Please wait...");


    }

    @Override
    public void show() {
        super.show();
    }
}
