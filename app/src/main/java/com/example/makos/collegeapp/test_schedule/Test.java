package com.example.makos.collegeapp.test_schedule;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.makos.collegeapp.R;

/**
 * Created by pc2 on 10-12-2018.
 */

public class Test extends Fragment {
    int value;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.test, container, false);

        Bundle pos = getArguments();
        value= Integer.parseInt(pos.getString("value"));
        Log.e("value", String.valueOf(value));
        return view;
    }
}
