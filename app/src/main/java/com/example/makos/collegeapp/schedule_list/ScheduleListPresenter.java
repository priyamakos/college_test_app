package com.example.makos.collegeapp.schedule_list;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.makos.collegeapp.NetworkInfo.Connectivity;
import com.example.makos.collegeapp.mysingleton.Mysingleton;
import com.example.makos.collegeapp.staticConstants.staticConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.example.makos.collegeapp.statusurl.Staticurl.schedule_test_list;

public class ScheduleListPresenter {
    Context context;
    Schedule_List schedule_list;
    ArrayList<scheduleListModel> scheduleListModels;
    public ScheduleListPresenter(Context context,Schedule_List schedule_list)
    {
        this.context = context;
        this.schedule_list = schedule_list;
        scheduleListModels = new ArrayList<>();
    }
    void  setSchedule_listapi()
    {
        String url = schedule_test_list+"?student_id="+ staticConstants.stud_id;
        if(Connectivity.isConnected(context))
        {
        final JsonArrayRequest scheduleList = new JsonArrayRequest(Request.Method.GET,url,null,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray jsonObject) {

                        Log.e("jsonObject", String.valueOf(jsonObject));
                        try {
                            if(jsonObject.getJSONObject(0).getString("Status").equals("True"))
                            {
                                schedule_list.scheduleList(fetch_scheduleList(jsonObject.getJSONObject(0).getJSONArray("Scheduled Exam List Data")));
                            }
                            else {
                                Toast.makeText(context,jsonObject.getJSONObject(0).getString("Message"),Toast.LENGTH_SHORT).show();
                          schedule_list.hideProgressbar();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {


                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            Log.e("jsonError",jsonError);
                            try {
                                JSONObject jsonObject = new JSONObject(jsonError);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                });
        Mysingleton.getInstance(context).addToRequestQueue(scheduleList);
    }
        else {
        Toast.makeText(context,"Please Check Internet",Toast.LENGTH_SHORT).show();
    }
    }

    private ArrayList<scheduleListModel> fetch_scheduleList(JSONArray scheduled_exam_list_data) {
      scheduleListModels.clear();
        for (int i=0;i<scheduled_exam_list_data.length();i++)
        {

            try {
                JSONObject itms = scheduled_exam_list_data.getJSONObject(i);
                if(itms.getString("is_attempted").equals("Exam Is Ongoing")) {
                    scheduleListModel model = new scheduleListModel();
                    model.setExam_date(itms.getString("exam_date"));
                    model.setExam_name(itms.getString("exam_name"));
                    model.setTest_name(itms.getString("test_name"));
                    model.setExam_time(itms.getString("exam_time"));
                    model.setIs_attempted(itms.getString("is_attempted"));
                    model.setTest_id(itms.getString("test_id"));
                    model.setExam_id(itms.getString("exam_id"));
                    scheduleListModels.add(model);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return scheduleListModels;
    }

    public interface ScheduleListener{
        void showProgressbar();
        void  hideProgressbar();
     void scheduleList(ArrayList<scheduleListModel> scheduleListModels);
    }
}
