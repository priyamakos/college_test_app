package com.example.makos.collegeapp.schedule;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.makos.collegeapp.NetworkInfo.Connectivity;
import com.example.makos.collegeapp.mysingleton.Mysingleton;
import com.example.makos.collegeapp.staticConstants.staticConstants;
import com.example.makos.collegeapp.statusurl.Staticurl;
import com.example.makos.collegeapp.subject.SubjectlistAdaptor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class SchedulePresenter {

    Context context;
    Schedule schedule;
    public SchedulePresenter(Context context,Schedule schedule)
    {
      this.context = context;
      this.schedule = schedule;
    }
    public void schdule_api(String testid,String examtime,String examdate,String studentid)
    {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat time = new SimpleDateFormat("HH:mm");
        try {

               SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
               SimpleDateFormat time1 = new SimpleDateFormat("HH:mm:ss");
               examdate = dateFormat1.format(dateFormat.parse(examdate));
               examtime = time1.format(time.parse(examtime));

        String url = Staticurl.schedule_test+"?student_id="+ staticConstants.stud_id+"&exam_id="+SubjectlistAdaptor.exam_id.replace(" ","%20")+
                                                    "&test_id="+testid.replace(" ","%20")+
                                                    "&exam_time="+examtime.replace(" ","%20")+
                                                    "&exam_date="+examdate.replace(" ","%20");
             Log.d(context.getPackageName()+"schdule_url",url);

        if(Connectivity.isConnected(context))
        {
            JsonArrayRequest examlistrequest = new JsonArrayRequest(Request.Method.GET,url,null,
                    new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray jsonObject) {

                            Log.e("jsonObject", String.valueOf(jsonObject));
                            try {

                             if(jsonObject.getJSONObject(0).getString("Status").equals("True"))
                             {
                                 schedule.message(fetct_detais(jsonObject.getJSONObject(0)),jsonObject.getJSONObject(0).getString("Status"));
                                 schedule.hideProgressBar();
                             }
                             else {
                                     Toast.makeText(context,jsonObject.getJSONObject(0).getString("Message"),Toast.LENGTH_SHORT).show();
                                     schedule.hideProgressBar();
                                 }

                            } catch (Exception e) {
                                Log.d("jsonerror",e.getMessage());
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error)
                        {
                            //chapter_list.hideProgressBar();

                            NetworkResponse networkResponse = error.networkResponse;
                            if (networkResponse != null && networkResponse.data != null) {
                                String jsonError = new String(networkResponse.data);
                                Log.e("jsonError",jsonError);
                                try {
                                    JSONObject jsonObject = new JSONObject(jsonError);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                    });
           Mysingleton.getInstance(context).addToRequestQueue(examlistrequest);
        }
        else {
            Toast.makeText(context,"Please Check Internet",Toast.LENGTH_SHORT).show();
        }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
    public String fetct_detais(JSONObject obj)
    {
        String response="";
        try {
            response = obj.getString("Message");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return  response;
    }
    public interface  Schedulelistener{
        void showProgressBar();
        void hideProgressBar();
        void message(String message,String status);
        // void show_tests(ArrayList<testlistModel> models);
    }
}
