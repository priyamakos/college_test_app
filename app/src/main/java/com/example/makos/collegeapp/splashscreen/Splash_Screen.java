package com.example.makos.collegeapp.splashscreen;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import com.example.makos.collegeapp.MainActivity;
import com.example.makos.collegeapp.R;
import com.example.makos.collegeapp.login.Login;
import com.example.makos.collegeapp.staticConstants.staticConstants;
import com.example.makos.collegeapp.statusurl.Staticurl;

/**
 * Created by pc2 on 08-12-2018.
 */

public class Splash_Screen extends AppCompatActivity {

    private Handler handler;
    SharedPreferences pref;
    int PERMISSION_ALL = 1;
    String[] PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.CAMERA
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        pref = getSharedPreferences(staticConstants.TAG,MODE_PRIVATE);
        if(pref != null)
        staticConstants.stud_id = pref.getString(staticConstants.student_id,"");

        if(!hasPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }
        handler = new Handler();
        handler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                    if(staticConstants.stud_id.equals("")) {
                        Intent splashLogin = new Intent(Splash_Screen.this, Login.class);
                        startActivity(splashLogin);
                        finish();
                    }else {
                        Intent splashLogin = new Intent(Splash_Screen.this, MainActivity.class);
                        startActivity(splashLogin);
                        finish();
                    }

            }
        },1800);
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
    }

