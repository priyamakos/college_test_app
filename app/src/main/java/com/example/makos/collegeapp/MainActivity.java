package com.example.makos.collegeapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Base64;
import android.view.MenuItem;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.makos.collegeapp.Attempt_Test.Attempt_Test;
import com.example.makos.collegeapp.change_password.Change_password;
import com.example.makos.collegeapp.history.History;
import com.example.makos.collegeapp.login.Login;
import com.example.makos.collegeapp.logout.logoutPresenter;
import com.example.makos.collegeapp.profile.Profile;
import com.example.makos.collegeapp.schedule_list.Schedule_List;
import com.example.makos.collegeapp.staticConstants.staticConstants;
import com.example.makos.collegeapp.subject.Subjectlist;
import com.example.makos.collegeapp.test_schedule.Test_Schedule;

import static com.example.makos.collegeapp.statusurl.Staticurl.logout;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, logoutPresenter.logout_listener
{
    NavigationView navigationView;
    DrawerLayout drawerLayout;
    Toolbar toolbar;
    String student_id;
    SharedPreferences pref;
    SharedPreferences.Editor editor;

ImageView rprofimg;
    CardView schedule_list,test_schedule,history,profile;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_drawer);
         toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        pref = getSharedPreferences(staticConstants.TAG,MODE_PRIVATE);

       schedule_list = findViewById(R.id.cardview3);
        test_schedule=findViewById(R.id.cardview2);
        history=findViewById(R.id.cardview4);
        profile=findViewById(R.id.cardview);

        Intent intent = getIntent();
        student_id = intent.getStringExtra("StudentId");

        schedule_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(MainActivity.this, Schedule_List.class);
                startActivity(intent);
            }
        });

        test_schedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Subjectlist.class);
                startActivity(intent);
            }
        });

        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, History.class);
                startActivity(intent);
            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Profile.class);
                startActivity(intent);
            }
        });

        drawerLayout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        //drawerLayout.openDrawer(GravityCompat.START);
        toggle.syncState();
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        final NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        rprofimg = header.findViewById(R.id.rprofimg);

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        switch (id) {


            case R.id.home:

                Intent back = new Intent(MainActivity.this,MainActivity.class);
                back.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(back);
                break;

            case R.id.change_password:

                Intent cPassword = new Intent(MainActivity.this,Change_password.class);
                cPassword.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                cPassword.putExtra("student_id",student_id);
                startActivity(cPassword);
                break;

            case R.id.logout:

                      logoutPresenter logoutPresenter = new logoutPresenter(MainActivity.this,this);
                      logoutPresenter.logout_api();
                break;

        }
        DrawerLayout drawer=(DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void logout_status(String message) {

        Toast.makeText(MainActivity.this,message,Toast.LENGTH_SHORT).show();
        editor = pref.edit();
        editor.clear();
        editor.commit();

        Intent intent = new Intent(MainActivity.this, Login.class);
        startActivity(intent);
        finish();

    }

    @Override
    protected void onResume() {

        if(pref.contains(staticConstants.profileimg))
        {
            if(!pref.getString(staticConstants.profileimg,"").equals("")){
                byte[] decodedString = Base64.decode(pref.getString(staticConstants.profileimg,""), Base64.DEFAULT);
                Bitmap bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                rprofimg.setImageBitmap(bitmap);
            }
        }
        super.onResume();
    }

}
