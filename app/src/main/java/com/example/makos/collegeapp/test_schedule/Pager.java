package com.example.makos.collegeapp.test_schedule;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.view.View;

/**
 * Created by pc2 on 10-12-2018.
 */

public class Pager extends FragmentStatePagerAdapter {
  int  tabcount;
    public Pager(FragmentManager fm, int tabcount) {
        super(fm);
        this.tabcount=tabcount;
    }



    @Override
    public Fragment getItem(int i) {


        switch (i)
        {
            case 0:
              JeeFragment  fragment1 = new JeeFragment();
                return fragment1;

            case 1:
             Neetfragment   fragment2 = new Neetfragment();
                return fragment2;

            case 2:
                Cetfragment    fragment3 = new Cetfragment();
                return fragment3;
            default:

             return null;

        }

    }

    @Override
    public int getCount() {
        return tabcount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        super.getPageTitle(position);

        switch (position) {
            case 0:
                return "JEE";
            case 1:
                return "NEET";
            case 2:
                return "CET";
            default:
                return null;
        }

    }
}
