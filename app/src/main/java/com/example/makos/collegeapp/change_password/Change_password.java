package com.example.makos.collegeapp.change_password;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.makos.collegeapp.NetworkInfo.Connectivity;
import com.example.makos.collegeapp.R;
import com.example.makos.collegeapp.mysingleton.Mysingleton;
import com.example.makos.collegeapp.staticConstants.staticConstants;
import com.example.makos.collegeapp.statusurl.Staticurl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Change_password extends AppCompatActivity {

    EditText oldpassword,newpassword;
    Button submit;
    String student_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        oldpassword=findViewById(R.id.old_password);
        newpassword=findViewById(R.id.new_password);
        submit=findViewById(R.id.verify);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        Intent intent = getIntent();
        student_id = intent.getStringExtra("student_id");

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validation();
            }
        });

    }

    private void validation() {
        if(oldpassword.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),"Enter Old Password",Toast.LENGTH_SHORT).show();
        }else if(newpassword.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),"Enter New Password",Toast.LENGTH_SHORT).show();
        }else {
            Change_password_api();
        }
    }

    private void Change_password_api() {

        String changeurl = Staticurl.changed_password
                + "?student_id=" + staticConstants.stud_id
                + "&old_password=" + Html.fromHtml(oldpassword.getText().toString())
                + "&new_password=" + Html.fromHtml(newpassword.getText().toString());

        Log.d("changeurl", changeurl);

        if (Connectivity.isConnected(getApplicationContext()))
        {
            final StringRequest request = new StringRequest(Request.Method.GET, changeurl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    Log.d("Changepassresponce",response);
                    try
                    {
                        JSONArray jsonArray = new JSONArray(response);

                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        if (jsonObject.getString("Status").equals("True"))
                        {
                            Toast.makeText(getApplicationContext(),jsonObject.getString("Message"),Toast.LENGTH_SHORT).show();
                            oldpassword.setText("");
                            newpassword.setText("");
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(),jsonObject.getString("Message"),Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
        else
        {
            Toast.makeText(getApplicationContext(),"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
