package com.example.makos.collegeapp.login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.makos.collegeapp.MainActivity;
import com.example.makos.collegeapp.NetworkInfo.Connectivity;
import com.example.makos.collegeapp.R;
import com.example.makos.collegeapp.forgot_password.Forgot_password;
import com.example.makos.collegeapp.mysingleton.Mysingleton;
import com.example.makos.collegeapp.sign_up.Sign_UP;
import com.example.makos.collegeapp.staticConstants.staticConstants;
import com.example.makos.collegeapp.statusurl.Staticurl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pc2 on 10-12-2018.
 */

public class Login extends AppCompatActivity{
    EditText mobile_num,password;
    Button login;
    TextView register,fpassword;
    String student_id;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        login=findViewById(R.id.login);
        register=findViewById(R.id.register);
        mobile_num=findViewById(R.id.mobile_no);
        password=findViewById(R.id.password);
        fpassword=findViewById(R.id.forgotpassword);
        preferences = getSharedPreferences(staticConstants.TAG,MODE_PRIVATE);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validation();
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent regin=new Intent(getApplicationContext(), Sign_UP.class);
                startActivity(regin);
            }
        });

        fpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent regin=new Intent(getApplicationContext(), Forgot_password.class);
                startActivity(regin);
            }
        });
    }



    private void validation() {

        if(mobile_num.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),"Enter Mobile Number",Toast.LENGTH_SHORT).show();
        }else if(password.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),"Enter Password",Toast.LENGTH_SHORT).show();
        }else if(isValidMobile(mobile_num.getText().toString())){

            login_api();
         //   Intent in=new Intent(getApplicationContext(), MainActivity.class);
           // startActivity(in);
        }
    }

    private void login_api() {

        if(Connectivity.isConnected(getApplicationContext())){

            Log.d("mobile_num", String.valueOf(mobile_num)+ password);
            String login_auth= Staticurl.login+
                    "?username="+mobile_num.getText().toString()+
                    "&password="+password.getText().toString();

            Log.d("login", login_auth);


            StringRequest stringRequest=new StringRequest(Request.Method.GET,
                    login_auth,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.e("loginres",response);

                            try {
                                JSONArray jsonArray=new JSONArray(response);
                                JSONObject jsonObject=jsonArray.getJSONObject(0);
                                if(jsonObject.getString("Status").equals("True"))
                                {
                                   editor = preferences.edit();
                                    student_id = jsonObject.getString("User ID");
                                    editor.putString(staticConstants.student_id ,student_id);
                                    editor.commit();
                                    staticConstants.stud_id = student_id;
                                    Toast.makeText(getApplicationContext(),jsonObject.getString("Message"),Toast.LENGTH_SHORT).show();
                                    Intent intent=new Intent(Login.this,MainActivity.class);
                                    intent.putExtra("StudentId",student_id);
                                    startActivity(intent);
                                }
                                else {
                                    Toast.makeText(getApplicationContext(),jsonObject.getString("Message"),Toast.LENGTH_SHORT).show();

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }



                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            Mysingleton.getInstance(Login.this).addToRequestQueue(stringRequest);

        }

        else {
            Toast.makeText(getApplicationContext(), "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    private boolean isValidMobile(String phone) {
        boolean check=false;
        //if(!Pattern.matches("[a-zA-Z]+", phone))
        //{
        if(phone.length() == 10)// || phone.length() > 0
        {
            //if(phone.length() != 10)
            // {
            check = true;

            Log.d("motest", "false");

            //txtPhone.setError("Not Valid Number");
            // }
        } else {
            Toast.makeText(getApplicationContext(), "Mobile Number Is Not Valid", Toast.LENGTH_SHORT).show();
            Log.d("motest", "true");
            check = false;
        }
        //} else
        // {
        //   check=false;
        //}
        return check;
    }
}
