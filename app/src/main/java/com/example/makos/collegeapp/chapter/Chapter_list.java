package com.example.makos.collegeapp.chapter;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.example.makos.collegeapp.R;
import com.example.makos.collegeapp.question.Question;
import com.example.makos.collegeapp.schedule.Schedule;
import com.example.makos.collegeapp.subject.Subjectlist;

import java.util.ArrayList;

/**
 * Created by pc2 on 10-12-2018.
 */

public class Chapter_list extends AppCompatActivity implements ChapterPresenter.Chapterlistener {
    CardView cardview;
    RecyclerView rectests;
    RecyclerView.LayoutManager manager ;
    ChapterPresenter chapterPresenter;
    ArrayList<testlistModel> testlistModels;
   Intent intent;
    ProgressDialog dialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chapter_list);
        cardview=findViewById(R.id.cardview);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
       manager = new LinearLayoutManager(Chapter_list.this);
       intent = getIntent();
        dialog  = new ProgressDialog(Chapter_list.this);
        dialog.setTitle("Loading...");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("Please wait.....");
       /* cardview.setOnClickListener(new Vi
    /*    cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(), Schedule.class);
                startActivity(intent);
            }
        });*/
     rectests = findViewById(R.id.rectest);
     rectests.setLayoutManager(manager);
     chapterPresenter = new ChapterPresenter(Chapter_list.this,this);

    }

    @Override
    protected void onResume() {
        if(intent != null) {
            chapterPresenter.test_list_api(intent.getStringExtra("exam_id"));
        dialog.show();
        }
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showProgressBar() {

    }

    @Override
    public void hideProgressBar() {
            dialog.dismiss();
    }

    @Override
    public void show_tests(ArrayList<testlistModel> models) {
       // testlistModels = new ArrayList<>();
        chapterListAdaptor adaptor = new chapterListAdaptor(models,Chapter_list.this,chapterPresenter);
        rectests.setAdapter(adaptor);
        adaptor.notifyDataSetChanged();

    }
}
