package com.example.makos.collegeapp.q_answer;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.makos.collegeapp.NetworkInfo.Connectivity;
import com.example.makos.collegeapp.history.History;
import com.example.makos.collegeapp.history.historyHomemodel;
import com.example.makos.collegeapp.history.historyListModel;
import com.example.makos.collegeapp.mysingleton.Mysingleton;
import com.example.makos.collegeapp.question.Question;
import com.example.makos.collegeapp.staticConstants.staticConstants;
import com.example.makos.collegeapp.statusurl.Staticurl;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Q_AnswerPresenter {
    Context context;
    Q_Answer history;
    q_Answermodel historyListModel;
   historyHomemodel historyHomemodel;
    ArrayList<q_Answermodel> examlisthistory;
    ArrayList<historyHomemodel> historyHomemodels;
    public Q_AnswerPresenter(Context context,Q_Answer history)
    {
        this.context = context;
        this.history = history;
        examlisthistory = new ArrayList<>();
        historyHomemodels = new ArrayList<>();
    }
    void historylistapi(String exam_id,String test_id)
    {
        String url= Staticurl.baseurl+"/student_result?student_id="+ staticConstants.stud_id+"&exam_id="+exam_id+"&test_id="+test_id;

     //   String url= Staticurl.baseurl+"/student_result?student_id=1&exam_id=1&test_id=1";
        Log.d("url",url);
        if(Connectivity.isConnected(context))
        {
            JsonArrayRequest attendexamrequest = new JsonArrayRequest(Request.Method.GET,url,null,
                    new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray jsonObject) {

                            Log.e("jsonObject", String.valueOf(jsonObject));
                            try {
                                JSONObject obj = jsonObject.getJSONObject(0);
                                if(obj.getString("Status").equals("True")) {
                                    history.historyList(fetch_histoy(obj.getJSONArray("Student Exam Details Result"),obj.getJSONArray("Student Exam Details Result")));
                                    //   Log.d("in",obj.getJSONArray("Test").toString());

                                }
                                else {
                                    Toast.makeText(context,obj.getString("Message"),Toast.LENGTH_SHORT).show();
                                    history.hideProgressbar();
                                }

                            } catch (Exception e) {
                                Log.d("jsonerror",e.getMessage());
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error)
                        {
                            //chapter_list.hideProgressBar();

                            NetworkResponse networkResponse = error.networkResponse;
                            if (networkResponse != null && networkResponse.data != null) {
                                String jsonError = new String(networkResponse.data);
                                Log.e("jsonError",jsonError);
                                try {
                                    JSONObject jsonObject = new JSONObject(jsonError);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                    });
            Mysingleton.getInstance(context).addToRequestQueue(attendexamrequest);
        }
        else {
            Toast.makeText(context,"Please Check Internet",Toast.LENGTH_SHORT).show();
        }
    }
    private ArrayList<q_Answermodel> fetch_histoy(JSONArray student_exam_details_result, JSONArray student_exam_details_result1) {
        examlisthistory.clear();
        for (int i = 0; i < student_exam_details_result.length(); i++) {
            try {

                JSONObject objmodel = student_exam_details_result.getJSONObject(i);
               // objmodel.put("test_details", student_exam_details_result1);
                Gson gson = new Gson();
                historyListModel = gson.fromJson(objmodel.toString(), q_Answermodel.class);
                examlisthistory.add(historyListModel);

            } catch (JSONException e) {
                e.printStackTrace();
                return examlisthistory;
            }
        }
        return examlisthistory;
    }
    public interface q_answerliseners{
        void hideProgressbar();
        void showProgressbar();
        void historyList(ArrayList<q_Answermodel> historyListModels);
        void historyhomeList(ArrayList<historyHomemodel> historyHomemodels);
    }
}
