package com.example.makos.collegeapp.question;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.makos.collegeapp.R;
import com.example.makos.collegeapp.database.DatabaseHandler;
import com.example.makos.collegeapp.staticConstants.staticConstants;

import java.util.ArrayList;
import java.util.Arrays;

public class Question extends AppCompatActivity implements questionPresenter.questionlisteners {
    ProgressDialog dialog;
    RecyclerView recquetion;
    questionPresenter presenter;
    RecyclerView.LayoutManager layoutManager;
    questionAdaptor adaptor;
    Intent intent;
    Button next,previous;
  //  DatabaseHandler handler;
    ArrayList<questionModel> questionModels1;
  public static   int pos= 0;
   public String test_id,exam_id;
    public static   String selops[];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_question);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        dialog = new ProgressDialog(Question.this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setTitle("Loading");
        dialog.setMessage("Please wait....");

        recquetion = findViewById(R.id.recquestion);
        layoutManager = new LinearLayoutManager(Question.this);

        recquetion.setLayoutManager(layoutManager);

         intent = getIntent();
         if(intent != null)
         {
             test_id = intent.getStringExtra("test_id");
             exam_id = intent.getStringExtra("exam_id");
         }
        presenter = new questionPresenter(Question.this,this);

        next = findViewById(R.id.next);
        previous= findViewById(R.id.previous);
        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pos >= 0) {
                    presenter.fetch_first_question(--pos);
                }
            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pos < selops.length) {

                    selops[pos] = String.valueOf(questionAdaptor.qselflag);
                    Log.e("selopt", Arrays.toString(selops));
                    sumbit_anwers(selops[pos]);
                    presenter.fetch_first_question(++pos);
                }

                if(next.getText().toString().equalsIgnoreCase("submit"))
                {

                    save_result(exam_id,test_id);

                }
            }
        });

 /* previous.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

      }
  });*/

    }

    private void save_result(String examid, String test_id) {
      presenter.save_result(examid,test_id);
    }

    private void sumbit_anwers(String ans) {
        presenter.submit_question(test_id,exam_id, questionModels1.get(0).getQuestion_id(),ans);

        }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
      ///  handler.trucate_table();
        presenter.questionApi(exam_id,test_id);
        showProgressbar();
    }

    @Override
    public void hideProgressbar() {
        dialog.dismiss();

    }

    @Override
    public void showProgressbar() {
        dialog.show();
    }

    @Override
    public void questionbody(ArrayList<questionModel> questionModels,String btntxt) {
        questionModels1 = questionModels;
        adaptor = new questionAdaptor(Question.this,questionModels,presenter,pos);
        adaptor.notifyDataSetChanged();
        recquetion.setAdapter(adaptor);
        next.setText(btntxt);
        hideProgressbar();

    }
}
