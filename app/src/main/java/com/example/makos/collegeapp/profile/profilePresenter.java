package com.example.makos.collegeapp.profile;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.makos.collegeapp.NetworkInfo.Connectivity;
import com.example.makos.collegeapp.mysingleton.Mysingleton;
import com.example.makos.collegeapp.staticConstants.staticConstants;
import com.example.makos.collegeapp.statusurl.Staticurl;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class profilePresenter {
    Profile profileview;
    Context context;
  profileModel profileModel;
    public profilePresenter(Context context,Profile profileview)
    {
        this.context = context;
        this.profileview = profileview;
        profileModel = new profileModel();
    }
    void  profile_api()
    {
         String url = Staticurl.baseurl+"/get_user_profile?student_id="+ staticConstants.stud_id;
         Log.d(staticConstants.TAG+"/url",url);
        if(Connectivity.isConnected(context))
        {
            JsonArrayRequest examlistrequest = new JsonArrayRequest(Request.Method.GET,url,null,
                    new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray jsonObject) {

                            Log.e("jsonObject", String.valueOf(jsonObject));
                            try {
                              if(jsonObject.getJSONObject(0).getString("Status").equals("True")) {
                                  fetch_profile(jsonObject.getJSONObject(0).getJSONArray("Student Profile List Data"));
                                  profileview.showprofile(profileModel);
                                 // profileview.hideProgressbar();
                              }
                              else {
                                  Toast.makeText(context,jsonObject.getJSONObject(0).getString("Message"),Toast.LENGTH_SHORT).show();
                              }

                            } catch (Exception e) {
                                Log.d("jsonerror",e.getMessage());
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error)
                        {
                            profileview.hideProgressbar();

                            NetworkResponse networkResponse = error.networkResponse;
                            if (networkResponse != null && networkResponse.data != null) {
                                String jsonError = new String(networkResponse.data);
                                Log.e("jsonError",jsonError);
                                try {
                                    JSONObject jsonObject = new JSONObject(jsonError);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                    });
            Mysingleton.getInstance(context).addToRequestQueue(examlistrequest);
        }
        else {
            Toast.makeText(context,"Please Check Internet",Toast.LENGTH_SHORT).show();
        }
    }

    private void fetch_profile(JSONArray student_profile_list) {
        try {
            JSONObject profileobj = student_profile_list.getJSONObject(0);
            Gson gson = new Gson();
            profileModel =gson.fromJson(profileobj.toString(), profileModel.class);

        } catch (JSONException e) {
            Log.d("jsonerror",e.getMessage());
            e.printStackTrace();
        }
    }

    public interface profilelistener{
        void  hideProgressbar();
        void  showProgressbar();
        void  showprofile(profileModel model);
    }
}
