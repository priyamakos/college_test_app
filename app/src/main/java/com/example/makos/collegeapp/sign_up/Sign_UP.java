package com.example.makos.collegeapp.sign_up;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.makos.collegeapp.NetworkInfo.Connectivity;
import com.example.makos.collegeapp.R;
import com.example.makos.collegeapp.login.Login;
import com.example.makos.collegeapp.mysingleton.Mysingleton;
import com.example.makos.collegeapp.staticConstants.staticConstants;
import com.example.makos.collegeapp.statusurl.Staticurl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by pc2 on 10-12-2018.
 */

public class Sign_UP extends AppCompatActivity {
    EditText school_name,mobile_num,password,student_name,email_id;
    Button register;
    Intent   intent;
    TextInputLayout textInputLayout;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
        register=findViewById(R.id.register);
        school_name=findViewById(R.id.school_name);
        mobile_num=findViewById(R.id.mobile_no);
        password=findViewById(R.id.password);
        student_name=findViewById(R.id.student_name);
        email_id=findViewById(R.id.email_id);
        intent = getIntent();
        textInputLayout = findViewById(R.id.passwordtxt);
        // Edit or register decision making
        if(intent != null)
        {
            showdetails(intent);
        }
        else {
            textInputLayout.setVisibility(View.VISIBLE);
        }

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validation();

            }
        });

    }

    private void showdetails(Intent intent) {

        register.setText("Save");
        textInputLayout.setVisibility(View.GONE);
         school_name.setText(intent.getStringExtra("school_name"));
         mobile_num.setText(intent.getStringExtra("mobile_no"));
         student_name.setText(intent.getStringExtra("student_name"));
         email_id.setText(intent.getStringExtra("email_id"));
    }

    private void validation() {

        if(student_name.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),"Enter Student Name",Toast.LENGTH_SHORT).show();
        }else if(school_name.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),"Enter School Name",Toast.LENGTH_SHORT).show();
        }else if(email_id.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),"Enter Email Id",Toast.LENGTH_SHORT).show();
        }else{
            if(!isValidMail(email_id.getText().toString())){
            Toast.makeText(getApplicationContext(),"Enter Valid Email ID",Toast.LENGTH_SHORT).show();
        } else if(mobile_num.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),"Enter Mobile Number",Toast.LENGTH_SHORT).show();
        }else if(isValidMobile(mobile_num.getText().toString())){
             if(register.getText().toString().equalsIgnoreCase("save"))
             {
                 update_profile();
             }
             else {
                 signup_api();
             }
          /*  Intent in=new Intent(getApplicationContext(), Login.class);
            startActivity(in);*/
        }}
    }

    private void update_profile() {
        if(Connectivity.isConnected(getApplicationContext())){

            final String reg_auth=Staticurl.update_profile+
                    "?student_name="+ Html.fromHtml(student_name.getText().toString().replace(" ","%20"))+
                    "&college_name="+Html.fromHtml(school_name.getText().toString().replace(" ","%20"))+
                    "&mobile_no="+Html.fromHtml(mobile_num.getText().toString().replace(" ","%20"))+
                    "&email_id="+Html.fromHtml(email_id.getText().toString().replace(" ","%20"))+
                    "&student_id="+ staticConstants.stud_id;

            Log.e("update", reg_auth);


            StringRequest stringRequest=new StringRequest(Request.Method.GET,
                    reg_auth,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("res",response);
                            try {
                                JSONArray jsonArray=new JSONArray(response);
                                if(jsonArray.getJSONObject(0).getString("Status").equals("True"))
                                {
                                  Toast.makeText(Sign_UP.this,jsonArray.getJSONObject(0).getString("Message"),Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            Mysingleton.getInstance(Sign_UP.this).addToRequestQueue(stringRequest);

        }

        else {
            Toast.makeText(getApplicationContext(), "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isValidMail(String email) {
        boolean check;
        Pattern p;
        Matcher m;

        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        p = Pattern.compile(EMAIL_STRING);

        m = p.matcher(email);
        check = m.matches();

        if(!check) {
            Toast.makeText(getApplicationContext(),"Enter Valid email id",Toast.LENGTH_SHORT).show();
            // txtEmail.setError("Not Valid Email");
        }
        return check;
    }
    private void signup_api() {

        if(Connectivity.isConnected(getApplicationContext())){

            String reg_auth= Staticurl.signup+
                    "?student_name="+ Html.fromHtml(student_name.getText().toString())+
                    "&college_name="+Html.fromHtml(school_name.getText().toString())+
                    "&mobile_no="+Html.fromHtml(mobile_num.getText().toString())+
                    "&email_id="+Html.fromHtml(email_id.getText().toString())+
                    "&password="+Html.fromHtml(password.getText().toString());

            Log.e("login", reg_auth);


            StringRequest stringRequest=new StringRequest(Request.Method.GET,
                    reg_auth,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                              Log.e("res",response);
                            try {
                                JSONArray jsonArray=new JSONArray(response);
                                JSONObject jsonObject=jsonArray.getJSONObject(0);
                                if(jsonObject.getString("Status").equals("True"))
                                {
                                    Toast.makeText(getApplicationContext(),jsonObject.getString("Message"),Toast.LENGTH_SHORT).show();
                                    Intent intent=new Intent(Sign_UP.this,Login.class);
                                    startActivity(intent);
                                }
                                else {
                                    Toast.makeText(getApplicationContext(),jsonObject.getString("Message"),Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            Mysingleton.getInstance(Sign_UP.this).addToRequestQueue(stringRequest);

        }

        else {
            Toast.makeText(getApplicationContext(), "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }



    private boolean isValidMobile(String phone) {
        boolean check=false;
        //if(!Pattern.matches("[a-zA-Z]+", phone))
        //{
        if(phone.length() == 10)// || phone.length() > 0
        {
            //if(phone.length() != 10)
            // {
            check = true;

            Log.d("motest", "false");

            //txtPhone.setError("Not Valid Number");
            // }
        } else {
            Toast.makeText(getApplicationContext(), "Mobile Number Is Not Valid", Toast.LENGTH_SHORT).show();
            Log.d("motest", "true");
            check = false;
        }
        //} else
        // {
        //   check=false;
        //}
        return check;
    }
}
