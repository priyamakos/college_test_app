package com.example.makos.collegeapp.history;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.view.MenuItem;

import com.example.makos.collegeapp.R;

import java.util.ArrayList;

/**
 * Created by pc2 on 10-12-2018.
 */

public class History extends AppCompatActivity implements Historypresenter.historyliseners {
    ProgressDialog dialog;
    Historypresenter historypresenter;
    RecyclerView rechistorylist;
    RecyclerView.LayoutManager manager;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        historypresenter = new Historypresenter(History.this,this);

        dialog = new ProgressDialog(History.this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setTitle("Loading");
        dialog.setMessage("Please wait....");

        rechistorylist = findViewById(R.id.rechistory);
        manager = new LinearLayoutManager(History.this);
        rechistorylist.setLayoutManager(manager);

    }

    @Override
    protected void onResume() {
        super.onResume();
        historypresenter.historyhomeapi();
        showProgressbar();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void hideProgressbar() {

    dialog.dismiss();
    }

    @Override
    public void showProgressbar() {
        dialog.show();

    }

    @Override
    public void historyhomeList(ArrayList<historyHomemodel> historyHomemodels) {
          historyHomeadaptor homeadaptor = new historyHomeadaptor(History.this,historyHomemodels,historypresenter);
          rechistorylist.setAdapter(homeadaptor);
          homeadaptor.notifyDataSetChanged();
          hideProgressbar();
    }

    @Override
    public void historyList(ArrayList<historyListModel> historyListModels) {

            historyListAadptor historyListAadptor = new historyListAadptor(History.this,historyListModels);
          //  rechistorylist.setAdapter(historyListAadptor);
           // historyListAadptor.notifyDataSetChanged();
        //    hideProgressbar();
    }
}
