package com.example.makos.collegeapp.logout;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.makos.collegeapp.MainActivity;
import com.example.makos.collegeapp.NetworkInfo.Connectivity;
import com.example.makos.collegeapp.mysingleton.Mysingleton;
import com.example.makos.collegeapp.staticConstants.staticConstants;
import com.example.makos.collegeapp.statusurl.Staticurl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class logoutPresenter {
    Context context;
    MainActivity mainview;

    public logoutPresenter(Context context, MainActivity mainview)
    {
        this.context = context;
        this.mainview = mainview;
    }
  public   void logout_api()
    {
      String url = Staticurl.logout+ staticConstants.stud_id;
        if(Connectivity.isConnected(context))
        {
            JsonArrayRequest examlistrequest = new JsonArrayRequest(Request.Method.GET,url,null,
                    new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray jsonObject) {

                            Log.e("jsonObject", String.valueOf(jsonObject));
                            try {
                                JSONObject obj = jsonObject.getJSONObject(0);
                                if(obj.getString("Status").equals("True"))
                                {
                                    mainview.logout_status(obj.getString("Message"));
                                }
                                else {
                                    mainview.logout_status(obj.getString("Message"));
                                }


                            } catch (Exception e) {
                                Log.d("jsonerror",e.getMessage());
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error)
                        {
                           // chapter_list.hideProgressBar();

                            NetworkResponse networkResponse = error.networkResponse;
                            if (networkResponse != null && networkResponse.data != null) {
                                String jsonError = new String(networkResponse.data);
                                Log.e("jsonError",jsonError);
                                try {
                                    JSONObject jsonObject = new JSONObject(jsonError);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                    });
            Mysingleton.getInstance(context).addToRequestQueue(examlistrequest);
        }
        else {
            Toast.makeText(context,"Please Check Internet",Toast.LENGTH_SHORT).show();
        }
    }
    public interface  logout_listener {

        void logout_status(String message);
    }
}
