package com.example.makos.collegeapp.result;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.makos.collegeapp.R;
import com.example.makos.collegeapp.question.questionModel;
import com.example.makos.collegeapp.question.questionPresenter;

import java.util.ArrayList;

public class resultAdaptor extends RecyclerView.Adapter<resultAdaptor.MyViewHolder> {
    resultPresenter presenter;
    Context context;
    int sr_q=0;
    ArrayList<resultmodel> resultmodels;
    TextView q_num1,q_num2,q_num3,q_num4;
    TextView question_id,ans1,ans2,ans3,ans4,question,ans,sel_ans;
   // questionPresenter presenter;
    public resultAdaptor(Context context, ArrayList<resultmodel> resultmodels,resultPresenter presenter)
    {
        this.presenter = presenter;
        this.context = context;
        this.resultmodels = resultmodels;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_result_layout,viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        question_id = myViewHolder.question_id;
        question = myViewHolder.question;
         sr_q = sr_q+1;

         ans1 = myViewHolder.ans1;
        ans2 = myViewHolder.ans2;
        ans3 = myViewHolder.ans3;
        ans4 = myViewHolder.ans4;
        ans = myViewHolder.ans;
        sel_ans = myViewHolder.sel_ans;

        q_num1 = myViewHolder.q_num1;
        q_num2 = myViewHolder.q_num2;
        q_num3 = myViewHolder.q_num3;
        q_num4 = myViewHolder.q_num4;

        resultmodel model = resultmodels.get(i);
        question_id.setText(String.valueOf(sr_q));
        question.setText(model.getQuestion_name());
        ans1.setText(model.getObjective1());
        ans2.setText(model.getObjective2());
        ans3.setText(model.getObjective3());
        ans4.setText(model.getObjective4());
        ans.setText(model.getSystem_answer());
        sel_ans.setText(model.getStudent_answer());
     if(model.getStudent_answer().equalsIgnoreCase(model.getSystem_answer()))
     {
         sel_ans.setTextColor(context.getResources().getColor(R.color.green));

     }
     else {
         sel_ans.setTextColor(context.getResources().getColor(R.color.red));
     }
    }

    @Override
    public int getItemCount() {
        return resultmodels.size();
    }

    public class  MyViewHolder extends RecyclerView.ViewHolder{
        TextView question_id,ans1,ans2,ans3,ans4,question;
        TextView q_num1,q_num2,q_num3,q_num4,ans,sel_ans;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            question = itemView.findViewById(R.id.question);
            question_id = itemView.findViewById(R.id.question_no);
            ans1 = itemView.findViewById(R.id.ans1);
            ans2 = itemView.findViewById(R.id.ans2);
            ans3 = itemView.findViewById(R.id.ans3);
            ans4 = itemView.findViewById(R.id.ans4);
            q_num1 = itemView.findViewById(R.id.q_num1);
            q_num2 = itemView.findViewById(R.id.q_num2);
            q_num3 = itemView.findViewById(R.id.q_num3);
            q_num4 = itemView.findViewById(R.id.q_num4);
            ans = itemView.findViewById(R.id.answer_opt);
            sel_ans = itemView.findViewById(R.id.sel_answer_opt);
        }
    }

}
