package com.example.makos.collegeapp.test_schedule;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TableLayout;

import com.example.makos.collegeapp.R;

/**
 * Created by pc2 on 10-12-2018.
 */

public class Test_Schedule extends AppCompatActivity implements TabLayout.BaseOnTabSelectedListener {
    public TabLayout tabLayout;
    public ViewPager viewPager;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_schedule);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        tabLayout =  findViewById(R.id.tabLayout);
        ViewPager viewPager =  findViewById(R.id.pager);
        Pager adapter = new Pager(getSupportFragmentManager(),3);
        viewPager.setAdapter(adapter);

        tabLayout.setupWithViewPager(viewPager);



        //Adding onTabSelectedListener to swipe views addOnTabSelectedListener
        tabLayout.setOnTabSelectedListener(this);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

        if (viewPager != null) {
            viewPager.setCurrentItem(tab.getPosition());
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
