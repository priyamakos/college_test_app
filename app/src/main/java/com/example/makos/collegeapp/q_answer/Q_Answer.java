package com.example.makos.collegeapp.q_answer;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.makos.collegeapp.R;
import com.example.makos.collegeapp.history.History;
import com.example.makos.collegeapp.history.Historypresenter;
import com.example.makos.collegeapp.history.historyHomeadaptor;
import com.example.makos.collegeapp.history.historyHomemodel;
import com.example.makos.collegeapp.history.historyListModel;

import java.util.ArrayList;

public class Q_Answer extends AppCompatActivity implements Q_AnswerPresenter.q_answerliseners {
Q_AnswerPresenter q_answerPresenter;
ProgressDialog dialog;
RecyclerView rechistorylist;
String exam_id,test_id;
Intent intent;
LinearLayoutManager manager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_q__answer);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        q_answerPresenter = new Q_AnswerPresenter(Q_Answer.this,this);

        dialog = new ProgressDialog(Q_Answer.this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setTitle("Loading");
        dialog.setMessage("Please wait....");

        rechistorylist = findViewById(R.id.recexamresult);
        manager = new LinearLayoutManager(Q_Answer.this);
        rechistorylist.setLayoutManager(manager);
        intent = getIntent();
        if(intent != null)
        {
            exam_id = intent.getStringExtra("exam_id");
            test_id = intent.getStringExtra("test_id");
        }

    }
    @Override
    protected void onResume() {
        super.onResume();
        q_answerPresenter.historylistapi(exam_id,test_id);
        showProgressbar();
    }

    @Override
    public void hideProgressbar() {
     dialog.dismiss();
    }

    @Override
    public void showProgressbar() {
dialog.show();
    }

    @Override
    public void historyList(ArrayList<q_Answermodel> historyListModels) {
        Q_listAadptor homeadaptor = new Q_listAadptor(Q_Answer.this,historyListModels,q_answerPresenter);
        rechistorylist.setAdapter(homeadaptor);
        homeadaptor.notifyDataSetChanged();
        hideProgressbar();
    }

    @Override
    public void historyhomeList(ArrayList<historyHomemodel> historyHomemodels) {

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
