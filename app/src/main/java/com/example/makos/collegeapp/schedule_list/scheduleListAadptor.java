package com.example.makos.collegeapp.schedule_list;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.makos.collegeapp.R;
import com.example.makos.collegeapp.history.historyHomemodel;
import com.example.makos.collegeapp.question.Question;
import com.example.makos.collegeapp.schedule.SchedulePresenter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class scheduleListAadptor extends RecyclerView.Adapter<scheduleListAadptor.MyViewHolder> {
    ArrayList<scheduleListModel> scheduleListModels;
    Context context;
    TextView time,date,test_name;
    Button start;
    public scheduleListAadptor(Context context, ArrayList<scheduleListModel> scheduleListModels, ScheduleListPresenter presenter)
    {
        this.scheduleListModels = scheduleListModels;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
     View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_schedule_list,viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder,final int i) {
       start = myViewHolder.start;
        test_name = myViewHolder.test_name;
        date = myViewHolder.date;
        time = myViewHolder.time;
       final scheduleListModel model = scheduleListModels.get(i);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy");

        try {
            Date dates = format.parse(model.getExam_date());
            date.setText(format1.format(dates));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        test_name.setText(model.getTest_name());
      //  date.setText(model.getExam_date());
        time.setText(model.getExam_time());
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(context, Question.class);
                in.putExtra("exam_id",scheduleListModels.get(i).getExam_id());
                in.putExtra("test_id",scheduleListModels.get(i).getTest_id());
                context.startActivity(in);

            }
        });

    }

    @Override
    public int getItemCount() {
        return scheduleListModels.size();
    }

    public class  MyViewHolder extends RecyclerView.ViewHolder{
 TextView test_name,date,time;
 Button start;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            test_name = itemView.findViewById(R.id.test_name);
            date = itemView.findViewById(R.id.date);
            time = itemView.findViewById(R.id.time);
            start = itemView.findViewById(R.id.start);

        }
    }
}
