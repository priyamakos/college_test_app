package com.example.makos.collegeapp.q_answer;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.makos.collegeapp.R;
import com.example.makos.collegeapp.history.historyListAadptor;
import com.example.makos.collegeapp.history.historyListModel;
import com.example.makos.collegeapp.result.Result;
import com.example.makos.collegeapp.result.resultmodel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class Q_listAadptor extends RecyclerView.Adapter<Q_listAadptor.MyViewHolder>  {
    TextView total_marks,date,test_name,filename,result;
    ArrayList<q_Answermodel> scheduleListModels;
    Context context;
    Q_AnswerPresenter presenter;
    public Q_listAadptor(Context context,ArrayList<q_Answermodel> scheduleListModels,Q_AnswerPresenter presenter)
    {
        this.context = context;
        this.presenter = presenter;
        this.scheduleListModels = scheduleListModels;
    }
    @NonNull
    @Override
    public Q_listAadptor.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_history_layout,viewGroup,false);
        return new Q_listAadptor.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Q_listAadptor.MyViewHolder myViewHolder, final int i) {
        test_name = myViewHolder.test_name;
        date = myViewHolder.date;
        filename = myViewHolder.filename;
        total_marks = myViewHolder.total_marks;
        result = myViewHolder.result;
        q_Answermodel model = scheduleListModels.get(i);
        test_name.setText(model.getTest_name());
        total_marks.setText(model.getTotal_marks()+"/"+model.getExam_marks());
        result.setText(model.getTest_result());
        filename.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                q_Answermodel model = scheduleListModels.get(i);
                Intent intent = new Intent(context, Result.class);
                Gson gson = new Gson();
                Type baseType = new TypeToken<ArrayList<resultmodel>>() {}.getType();
                String jsonstring = gson.toJson(model.getList1(),baseType);
                Log.d("jsonstring",jsonstring);
                intent.putExtra("resultmodel",jsonstring);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return scheduleListModels.size();
    }

    public class  MyViewHolder extends RecyclerView.ViewHolder{
        TextView test_name,exam_name,total_marks,exam_marks,date,filename,result;
        Button start;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            test_name = itemView.findViewById(R.id.test_name);
            date = itemView.findViewById(R.id.date);
            total_marks = itemView.findViewById(R.id.marks);
            filename = itemView.findViewById(R.id.filename);
            result = itemView.findViewById(R.id.result);

        }
    }
}
