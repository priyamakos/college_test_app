package com.example.makos.collegeapp.forgot_password;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.makos.collegeapp.NetworkInfo.Connectivity;
import com.example.makos.collegeapp.R;
import com.example.makos.collegeapp.mysingleton.Mysingleton;
import com.example.makos.collegeapp.statusurl.Staticurl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Forgot_password extends AppCompatActivity {

    EditText emailid;
    Button   submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        emailid=findViewById(R.id.email_id);
        submit=findViewById(R.id.verify);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validation();
            }
        });
    }

    private void validation() {

        if(emailid.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),"Enter Registered Email Id",Toast.LENGTH_SHORT).show();
        }else if(!isValidMail(emailid.getText().toString())){
            Toast.makeText(getApplicationContext(),"Enter Valid Email Id",Toast.LENGTH_SHORT).show();
        }else {
            forgot_password_api();
        }


    }


    private boolean isValidMail(String email) {
        boolean check;
        Pattern p;
        Matcher m;

        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        p = Pattern.compile(EMAIL_STRING);

        m = p.matcher(email);
        check = m.matches();

        if(!check) {
            Toast.makeText(getApplicationContext(),"Enter Valid email id",Toast.LENGTH_SHORT).show();
            // txtEmail.setError("Not Valid Email");
        }
        return check;
    }

    private void forgot_password_api() {
        String forgotpasswordurl = "http://collegeapp-env.9jthkatitt.ap-south-1.elasticbeanstalk.com/collegeapp/forgot_pass"
                +"?username="+Html.fromHtml(emailid.getText().toString());
        Log.d("changeurl", forgotpasswordurl);

        if (Connectivity.isConnected(getApplicationContext()))
        {
            final StringRequest request = new StringRequest(Request.Method.GET, forgotpasswordurl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    Log.d("forgotpassword",response);
                    try
                    {

                        JSONArray jsonArray = new JSONArray(response);

                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        if (jsonObject.getString("Status").equals("1"))
                        {
                            Toast.makeText(getApplicationContext(),jsonObject.getString("Message"),Toast.LENGTH_SHORT).show();

                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(),jsonObject.getString("Message"),Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
        else
        {
            Toast.makeText(getApplicationContext(),"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }

    }

}
