package com.example.makos.collegeapp.subject;

public class subjectModel {
    String exam_id,exam_name,exam_marks,no_of_questions,negative_marks;

    public String getExam_id() {
        return exam_id;
    }

    public void setExam_id(String exam_id) {
        this.exam_id = exam_id;
    }

    public String getExam_name() {
        return exam_name;
    }

    public void setExam_name(String exam_name) {
        this.exam_name = exam_name;
    }

    public String getExam_marks() {
        return exam_marks;
    }

    public void setExam_marks(String exam_marks) {
        this.exam_marks = exam_marks;
    }

    public String getNo_of_questions() {
        return no_of_questions;
    }

    public void setNo_of_questions(String no_of_questions) {
        this.no_of_questions = no_of_questions;
    }

    public String getNegative_marks() {
        return negative_marks;
    }

    public void setNegative_marks(String negative_marks) {
        this.negative_marks = negative_marks;
    }
}
