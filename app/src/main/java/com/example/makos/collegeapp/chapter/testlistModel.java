package com.example.makos.collegeapp.chapter;

public class testlistModel {
    String message,test_id,test_name,test_hrs;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTest_id() {
        return test_id;
    }

    public void setTest_id(String test_id) {
        this.test_id = test_id;
    }

    public String getTest_name() {
        return test_name;
    }

    public void setTest_name(String test_name) {
        this.test_name = test_name;
    }

    public String getTest_hrs() {
        return test_hrs;
    }

    public void setTest_hrs(String test_hrs) {
        this.test_hrs = test_hrs;
    }
}
