package com.example.makos.collegeapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {


        private static final String TAG = "Collegeapp";
        private static final int DATABASE_VERSION =3;

        // Database Name
        private static final String DATABASE_NAME = "collegeapp.db";

        // exam table name
        private static final String TABLE_FORM_DATA = "examtable";

        // examtable Table Columns names

        private static final String STUDENT_ID = "student_id";
        private static final String EXAM_ID = "cpt_id";
        private static final String TEST_ID = "test_id";
        private static final String Q_NO = "q_no";
        private static final String S_ANS = "selected_ans";
    private static final String ATTENDED = "attended";

        private Context context;

        public DatabaseHandler(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            this.context=context;
        }

        // Creating Tables

        @Override
        public void onCreate(SQLiteDatabase db) {

            String tablexam="create table if not exists examtable(id INTEGER PRIMARY KEY AUTOINCREMENT,student_id TEXT,cpt_id TEXT,test_id TEXT,q_no TEXT,selected_ans TEXT,attended TEXT)";
            db.execSQL(tablexam);

        }
        // Upgrading database
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // Drop older table if existed
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_FORM_DATA);
            onCreate(db);
        }

        public String last_attended_ques(String exam_id,String test_id)
        {
            SQLiteDatabase db = this.getWritableDatabase();
            String q_no1="";
            String query = "SELECT q_no FROM "+ TABLE_FORM_DATA+" WHERE attended = 0 AND "+EXAM_ID+"="+exam_id+" AND test_id="+test_id+" ORDER BY q_no" ;
            Cursor data = db.rawQuery(query, null);
            Log.d("count", String.valueOf(data.getCount()));
          if(this.getData1(exam_id,test_id).getCount() != data.getCount()) {
              if (data.moveToFirst()) {
                  q_no1 = data.getString(data.getColumnIndex("q_no"));
                  Log.d("q_no1", q_no1);
              }
          }
          else {
              q_no1 = "0";
          }
            return q_no1;

        }


        // Adding new Form
        public String addNewquestion(String student_id,String exam_id,String test_id,String q_no,String ans) {
            String val="false";
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(STUDENT_ID,student_id);
            values.put(EXAM_ID, exam_id);
            values.put(TEST_ID, test_id);
            values.put(Q_NO, q_no);
            values.put(S_ANS, ans);
            values.put(ATTENDED,"0");

Log.d("insert","loop");
            // Inserting Row
           if(getData(exam_id,test_id,q_no).getCount() == 0) {
                long rowInserted = db.insert(TABLE_FORM_DATA, null, values);
                if(rowInserted != -1){
                    val="true";
                }
                else
                {
                    val="false";

                }
            }

            db.close(); // Closing database connection
            return val;
        }


    public void updatequestionoption(String exam_id,String student_id,String test_id,String q_no,String ans) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(S_ANS,ans);
        values.put(ATTENDED,"1");
        ContentValues whereClause = new ContentValues();
        whereClause.put(STUDENT_ID,student_id);
        whereClause.put(TEST_ID,test_id);
        whereClause.put(EXAM_ID, exam_id);
        whereClause.put(Q_NO,q_no);


        Log.e("Count",values+"");
        db.update(TABLE_FORM_DATA, values,"q_no="+q_no+" AND "+TEST_ID+"="+test_id+" AND "+EXAM_ID+"="+exam_id,null);
     Log.d("whereclause","q_no="+q_no+" AND "+TEST_ID+"="+test_id+" AND "+EXAM_ID+"="+exam_id);
        db.close();
    }

        public Cursor getData(String exam_id,String test_id,String q_no){
            SQLiteDatabase db = this.getWritableDatabase();
            String query = "SELECT * FROM " + TABLE_FORM_DATA+" where q_no="+q_no+" AND "+TEST_ID+"="+test_id+" AND "+EXAM_ID+"="+exam_id;
            Cursor data = db.rawQuery(query, null);

            return data;
        }
    public Cursor getData1(String exam_id,String test_id){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_FORM_DATA+" where "+TEST_ID+"="+test_id+" AND "+EXAM_ID+"="+exam_id;
        Cursor data = db.rawQuery(query, null);
        Log.d("allcount", String.valueOf(data.getCount()));
        return data;
    }


        public void clearDatabase() {
            context.deleteDatabase(DATABASE_NAME);
        }
        public void  droptable(){
            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_FORM_DATA);

        }
        public void trucate_table(){
            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL("DELETE FROM "+TABLE_FORM_DATA);

    }

    }


