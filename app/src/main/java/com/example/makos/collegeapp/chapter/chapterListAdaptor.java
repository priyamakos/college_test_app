package com.example.makos.collegeapp.chapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.makos.collegeapp.R;
import com.example.makos.collegeapp.schedule.Schedule;
import com.example.makos.collegeapp.subject.SubjectlistAdaptor;

import java.util.ArrayList;

public class chapterListAdaptor extends RecyclerView.Adapter<chapterListAdaptor.ViewHolder> {

  ArrayList<testlistModel> testlistModels;
  Context context;
  public  chapterListAdaptor(ArrayList<testlistModel> testlistModels,Context context,ChapterPresenter presenter)
  {
      this.testlistModels = testlistModels;
      this.context = context;
  }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
       View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_testlist,viewGroup,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder,final int i) {
        TextView test = viewHolder.txttestname;
        testlistModel model = testlistModels.get(i);
        test.setText(model.getTest_name());
        test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Schedule.class);
                intent.putExtra("test_name",testlistModels.get(i).getTest_name());
                intent.putExtra("exam_name", SubjectlistAdaptor.exam_name);
                intent.putExtra("test_id",testlistModels.get(i).getTest_id());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return testlistModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
         TextView txttestname;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txttestname = itemView.findViewById(R.id.txt_test_name);
        }
    }
}
